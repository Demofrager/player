package cz.agents.gtlibrary.algorithms.mcts.nodes;

import cz.agents.gtlibrary.algorithms.mcts.distribution.Distribution;
import cz.agents.gtlibrary.interfaces.Action;
import cz.agents.gtlibrary.interfaces.GameState;
import cz.agents.gtlibrary.interfaces.Player;
import cz.agents.gtlibrary.strategy.Strategy;
import java.util.Arrays;

public class LeafNode extends NodeImpl {

	private final double[] utilities;
	private int nbSamples = 0;
    //<>
    private int selfRandomAct = 1;
    private int parentRandomAct = 1;
	
	
	public LeafNode(InnerNode parent, GameState gameState, Action lastAction) {
		super(parent, lastAction, gameState);
		this.utilities = gameState.getUtilities();
		this.selfRandomAct = parent.getSelfRandomAct();
		this.parentRandomAct = parent.getSelfRandomAct();
	}

	public static int leafNodeUsed = 0;

	
	//<><>
	@Override
	public int getSelfRandomAct(){
		return this.selfRandomAct;
	}
	
	@Override
	public int getParentRandomAct(){
		return this.parentRandomAct;
	}
	///
	
	
	@Override
	public double[] simulate() {
		leafNodeUsed++;
		return Arrays.copyOf(utilities, utilities.length);
	}

	@Override
	public void backPropagate(Action action, double[] value) {
		++nbSamples;
		parent.backPropagate(lastAction, value);
	}

	@Override
	public GameState getGameState() {
		return gameState;
	}

	public double[] getEV() {
		return utilities;
	}

	public int getNbSamples() {
		return nbSamples;
	}
	
	@Override
	public Node selectRecursively() {
		return this;
	}

	@Override
	public void expand() {
		
	}

	@Override
	public Strategy getStrategyFor(Player player, Distribution distribution, int cutOffDepth) {
		return algConfig.getEmptyStrategy();
	}
}
