package cz.agents.gtlibrary.algorithms.mcts.nodes;



import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import cz.agents.gtlibrary.algorithms.mcts.distribution.Distribution;
import cz.agents.gtlibrary.interfaces.Action;
import cz.agents.gtlibrary.interfaces.GameState;
import cz.agents.gtlibrary.interfaces.Player;
import cz.agents.gtlibrary.strategy.Strategy;


/**
 *  
 * @edited arminchitizadeh
 * I have added NodeValue
 */
public interface Node {

    public float getUtilityNormalisation();
    public int getNumberOfNodesInIS();
    public ArrayList<Integer> getFinalValueForTemps();
	
    public void setUtilityNormalisation(float input);
    public void setNumberOfNodesInIS(int input);
    public void addFinalValueForTemps(Integer input);
    
    
    public void setMultiNodeValue(double[] utils);
    
    public double[] getMultiNodeValue();
    
	public int getSelfRandomAct();
	
	public int getParentRandomAct();
	
	public Map<Action, Node> getArminChildren();
	
	public void putChild(Action inA, Node inN);
	
	public void incrementNodeLevel();

	public int getNodeLevel();
		
	public void setNodeValue( int nv);
	
	public int getNodeValue();
	
	public Node selectRecursively();

	public void expand();
	
	public double[] simulate();

	public void backPropagate(Action action, double[] value);

	public InnerNode getParent();

	public void setParent(InnerNode parent);
	
	public Action getLastAction();

	public GameState getGameState();

	public double[] getEV();

	public int getNbSamples();
    
	public int getDepth();
	
	public Strategy getStrategyFor(Player player, Distribution distribution);
        
    public Strategy getStrategyFor(Player player, Distribution distribution, int cutOffDepth);

}