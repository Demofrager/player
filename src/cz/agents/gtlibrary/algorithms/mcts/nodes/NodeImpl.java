package cz.agents.gtlibrary.algorithms.mcts.nodes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import cz.agents.gtlibrary.algorithms.mcts.MCTSConfig;
import cz.agents.gtlibrary.algorithms.mcts.distribution.Distribution;
import cz.agents.gtlibrary.interfaces.Action;
import cz.agents.gtlibrary.interfaces.GameState;
import cz.agents.gtlibrary.interfaces.Player;
import cz.agents.gtlibrary.strategy.Strategy;

public abstract class NodeImpl implements Node {
    
	protected Map<Action, Node> arminChildren;
	protected InnerNode parent;
	protected GameState gameState;
	protected Action lastAction;
	protected MCTSConfig algConfig;
	protected int depth;
	//<>
	protected int nodeValue;
	protected double[] multiNodeValue;
    private int level = 0;
    
    // this is bad design but is just for testing
    private float utilityNormalisation = -1;
    private int NumberOfNodesInIS = -1;
    private ArrayList<Integer> finalValueForTemps = new ArrayList<Integer>();

    
    //This is for cooperation game playing. In this one players know the sequence of moves by 
    // other players, for now they know the sequence of moves by  everyother player but the 
    // random player
	private LinkedList<Action> pastActions = new  LinkedList<Action>();
    
	
	public NodeImpl(InnerNode parent, Action lastAction, GameState gameState) {
		this.parent = parent;
		this.lastAction = lastAction;
		this.gameState = gameState;
		this.algConfig = parent.algConfig;
		depth = parent.depth + 1;
		//<>
		this.nodeValue = 0;
		this.level = parent.getNodeLevel() + 1;
		this.arminChildren = new HashMap<Action, Node>();
		// This is added after coordination thing
		if (this.parent != null ){
			if (parent.getPastActions()!= null){
				pastActions = new LinkedList<Action>(parent.getPastActions());
			}
		
			if (!this.parent.getGameState().isPlayerToMoveNature()){
				pastActions.add(lastAction);
			}
		}
	}

	public NodeImpl(MCTSConfig algConfig, GameState gameState) {
		this.algConfig = algConfig;
		this.gameState = gameState;
		depth = 0;
	}
	
	
	// It takes a node and copy attributes and fill the attributes that are empty
	/*
	public static NodeImpl NodeCopy(NodeImpl oldNode){
		NodeImpl newNode = new NodeImpl(oldNode);
		
		return newNode;
	}
	*/
	
	public LinkedList<Action> getPastActions(){
		return this.pastActions;
	}
 	
	public void setMultiNodeValue(double[] utils) {
		this.multiNodeValue = utils;
	}

	public double[] getMultiNodeValue() {		
		return this.multiNodeValue;
	}
	
    public float getUtilityNormalisation(){
    	return this.utilityNormalisation;
    }
    public int getNumberOfNodesInIS(){
    	return this.NumberOfNodesInIS;
    }
    public ArrayList<Integer> getFinalValueForTemps(){
    	return this.finalValueForTemps;
    }
	
    public void setUtilityNormalisation(float input){
    	this.utilityNormalisation = input;
    }
    public void setNumberOfNodesInIS(int input){
    	this.NumberOfNodesInIS = input;
    }
    public void addFinalValueForTemps(Integer input){
    	this.finalValueForTemps.add(input);
    }
	
	
	/**
	 * I have added level so I can generate the information set based on the level of the node and perception
	 * Remember that the player works the moves in a sequential form, meaning that it does not consider joint moves
	 * @return
	 */
	
	@Override
	public Map<Action, Node> getArminChildren(){
		return this.arminChildren;
	}
	
	@Override
	public void putChild(Action inA, Node inN){
		System.out.println("INSIDE PUTCHILD L1");
		if (this.arminChildren == null){
			System.out.println("Children are null");
			arminChildren = new HashMap<Action, Node>();
		}
		this.arminChildren.put(inA, inN);
		System.out.println("INSIDE PUTCHILD L2");
	}
	
	@Override
	public int getNodeLevel(){
		return level;
	}
	
	@Override
	public void incrementNodeLevel(){
		this.level = this.level + 1; 
	}
	
	
	@Override
	public void setNodeValue(int nv){
		this.nodeValue = nv;
	}
	
	@Override
	public int getNodeValue(){
		return this.nodeValue;
	}
	
	@Override
	public InnerNode getParent() {
		return parent;
	}
	
	@Override
	public int getDepth() {
		return depth;
	}
	
    @Override
    public void setParent(InnerNode parent) {
        this.parent = parent;
    }

	@Override
	public Action getLastAction() {
		return lastAction;
	}
	
	@Override
	public GameState getGameState() {
		return gameState;
	}
	
        @Override
	public Strategy getStrategyFor(Player player, Distribution distribution) {
            return getStrategyFor(player, distribution, Integer.MAX_VALUE);
        }
        
	@Override
	public String toString() {
		return "Node: " + gameState;
	}

}
