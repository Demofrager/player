package cz.agents.gtlibrary.ggp.gdlII.domain.propnet;

import cz.agents.gtlibrary.iinodes.ActionImpl;
import cz.agents.gtlibrary.interfaces.GameState;
import cz.agents.gtlibrary.interfaces.InformationSet;

public class GGPAction extends ActionImpl{
	
	private static final long serialVersionUID = -3149157783804816812L;

	private int actionsInState = 0;
	private int inputPropositionIndex = 0;
	
	private int hashCode = -1;
	
	public GGPAction(int inputPropositionIndex, InformationSet informationSet, int possibleActions){
		super(informationSet);
		this.inputPropositionIndex = inputPropositionIndex;
		this.actionsInState = possibleActions;
	}
	
	public int getInputPropositionIndex(){
		return inputPropositionIndex;
	}
	
	public int getRandomActionsAmount(){
		return actionsInState;
	}

	@Override
	public void perform(GameState gameState) {
		((GGPGameState)gameState).performAction(this);
	}

	@Override
	public int hashCode() {
		if (hashCode == -1) {
			final int prime = 31;
			int result = 1;
			
			result = prime * result + ((informationSet == null) ? 0 : informationSet.hashCode());
			result = prime * result + actionsInState;
			result = prime * result + inputPropositionIndex;
			hashCode = result;
		}
		return hashCode;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		GGPAction other = (GGPAction) obj;
		if (inputPropositionIndex != other.inputPropositionIndex)
			return false;
		if (actionsInState != other.actionsInState)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return GGPGameInfo.propNet.getInputProposition(inputPropositionIndex).getName().toString();
	}

}
