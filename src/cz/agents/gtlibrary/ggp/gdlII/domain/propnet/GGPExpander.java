package cz.agents.gtlibrary.ggp.gdlII.domain.propnet;

import java.util.ArrayList;
import java.util.List;

import org.ggp.base.util.propnet.architecture.components.Proposition;

import cz.agents.gtlibrary.iinodes.ExpanderImpl;
import cz.agents.gtlibrary.interfaces.Action;
import cz.agents.gtlibrary.interfaces.AlgorithmConfig;
import cz.agents.gtlibrary.interfaces.GameState;
import cz.agents.gtlibrary.interfaces.InformationSet;

public class GGPExpander<I extends InformationSet> extends ExpanderImpl<I> {

	private static final long serialVersionUID = 6223351509278328583L;

	public GGPExpander(AlgorithmConfig<I> algConfig) {
		super(algConfig);
	}

	@Override
	public List<Action> getActions(GameState gameState) {
		return getActions((GGPGameState)gameState);
	}
	
	private List<Action> getActions(GGPGameState gameState){
		List<Action> actions = new ArrayList<Action>();
		List<Proposition> props = new ArrayList<Proposition>();
		if (GGPGameInfo.DEBUG) System.out.println("[GENERATE: start] "+gameState.toString());
		for(Proposition p : GGPGameInfo.propNet.getLegalPropositions().get(gameState.getPlayerRoleToMove()))
			if(GGPGameInfo.readProposition(gameState, p)){
				if (GGPGameInfo.DEBUG) System.out.println(p.getName());
				props.add(p);
			}
		if (GGPGameInfo.DEBUG) System.out.println("[GENERATE: end] "+gameState.toString());
		if (GGPGameInfo.DEBUG) System.out.println();
		if(props.isEmpty())
			System.out.println("[Expander] Error: no possible actions");
		for(Proposition p : props)
			//actions.add(new GGPAction( gameState.getPlayerToMove(), p.getName(), null, props.size()  ));
			actions.add(new GGPAction(GGPGameInfo.propNet.getInputPropositionIndex(GGPGameInfo.propNet.getLegalInputMap().get(p)),getAlgorithmConfig().getInformationSetFor(gameState),props.size()));
		return actions;
	}

}
