package cz.agents.gtlibrary.ggp.gdlII.domain.propnet;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.ggp.base.util.gdl.grammar.Gdl;
import org.ggp.base.util.gdl.grammar.GdlConstant;
import org.ggp.base.util.propnet.architecture.Component;
import org.ggp.base.util.propnet.architecture.PropNet;
import org.ggp.base.util.propnet.architecture.components.And;
import org.ggp.base.util.propnet.architecture.components.Constant;
import org.ggp.base.util.propnet.architecture.components.Not;
import org.ggp.base.util.propnet.architecture.components.Or;
import org.ggp.base.util.propnet.architecture.components.Proposition;
import org.ggp.base.util.propnet.architecture.components.Transition;
import org.ggp.base.util.propnet.factory.PropNetFactory;
import org.ggp.base.util.statemachine.Role;

import com.rits.cloning.Cloner;

import cz.agents.gtlibrary.iinodes.PlayerImpl;
import cz.agents.gtlibrary.interfaces.GameInfo;
import cz.agents.gtlibrary.interfaces.Player;

public class GGPGameInfo implements GameInfo{
	
	// hra NEMUSI obsahovat random hrace (!!!)
	
	public static final boolean DEBUG = false;
	public static final boolean EXPORT = false;
	public static final String EXPORT_FILENAME = "card.dot";
	
	public static PropNet propNet;
    //private static List<Proposition> ordering;
    public static List<Role> roles;
    public static int randomPlayerIndex;
	
	public static Player[] ALL_PLAYERS;
	
	//public static boolean isOnePlayerGame;
	//public static boolean playersSwitched;
	
	public static int maxDepth;
	//public static EvaluationFunction heuristic;
	
	public static boolean boostedPerceptFinding;
	public static Role boostedPlayerIndex;
	
	public static PrintWriter writer;
	public static Cloner cloner;
	
	public GGPGameInfo(List<Gdl> description) throws FileNotFoundException, UnsupportedEncodingException{
		if(EXPORT) writer = new PrintWriter(EXPORT_FILENAME, "UTF-8");
		if(EXPORT) writer.println("digraph tree {");
        propNet = PropNetFactory.create(description);
        roles = propNet.getRoles();
		GGPGameInfo.maxDepth = Integer.MAX_VALUE;
		boostedPerceptFinding = false;
		
		relistRoles();
		
		ALL_PLAYERS = new Player[roles.size()];
		for(int i = 0; i < roles.size(); i++){
			//System.out.println(roles.get(i).toString());
			ALL_PLAYERS[i] = new PlayerImpl(i);
		}
		
		System.out.printf("[GameInfo] Random player index: %d.\n",randomPlayerIndex);
		
		//findRandom();
		//randomPlayerIndex=-1; // NATVRDO NENI RANDOM
		//maxDepth = 2;
		cloner = new Cloner();
	}
	
	public GGPGameInfo(List<Gdl> description, GdlConstant role) throws FileNotFoundException, UnsupportedEncodingException{
		this(description);
		boostedPerceptFinding = true;
		boostedPlayerIndex = roles.get(getPlayerIndexByRole(role));
	}
	
	
	private void relistRoles(){
		findRandom();
		if(randomPlayerIndex != -1){
			Collections.swap(roles, randomPlayerIndex, roles.size()-1);
			randomPlayerIndex = roles.size()-1;
		}
	}
	
	public static Player getPlayerByRole(GdlConstant role){
		for(int i = 0; i<roles.size(); i++)
			if(roles.get(i).getName().equals(role))
				return ALL_PLAYERS[i];
		return null;
	}
	
	public static int getPlayerIndexByRole(GdlConstant role){
		for(int i = 0; i<roles.size(); i++)
			if(roles.get(i).getName().equals(role))
				return i;
		return -1;
	}
	
	private void findRandom(){
		int i = 0;
		for(Role r : roles){
			if(r.getName().getValue().toLowerCase().equals("random")){
				randomPlayerIndex = i;
				return;
			}
			i++;
		}
		randomPlayerIndex = -1; // random player not present
	}
		

	@Override
	public double getMaxUtility() {
		return 100;
	}

	@Override
	public Player getFirstPlayerToMove() {
		return ALL_PLAYERS[0];
	}

	@Override
	public Player getOpponent(Player player) {
		return player.equals(ALL_PLAYERS[0]) ? ALL_PLAYERS[1] : ALL_PLAYERS[0];
		//return null;
	}

	@Override
	public String getInfo() {
		return "GGP Game";
	}

	@Override
	public int getMaxDepth() {
		return maxDepth;
	}

	@Override
	public Player[] getAllPlayers() {
		return ALL_PLAYERS;
	}
	
	public List<Proposition> getOrdering() // tohle je priprava na forward
	{
	    // List to contain the topological ordering.
	    List<Proposition> order = new LinkedList<Proposition>();
	    				
		// All of the components in the PropNet
		List<Component> components = new ArrayList<Component>(propNet.getComponents());
		System.out.println(components.size());
		
		// All of the propositions in the PropNet.
		List<Proposition> propositions = new ArrayList<Proposition>(propNet.getPropositions());
		System.out.println(propositions.size());
	    // TODO: Compute the topological ordering.		
		
		return order;
	}
	
	public void printPropnet(){
		List<Component> components = new ArrayList<Component>(propNet.getComponents());
		System.out.println(components.size());
		for(Component c : components){
			System.out.println(c.toString());
		}
		
		// All of the propositions in the PropNet.
		List<Proposition> propositions = new ArrayList<Proposition>(propNet.getPropositions());
		System.out.println(propositions.size());
		for(Proposition p : propositions){
			System.out.println(p.toString());
		}
	}
	/*
	public static void markBases(boolean[] vector){
		int i = 0;
		for(Proposition p : propNet.getBasePropositions().values()){
			p.setValue(vector[i]);
			i++;
		}
	}
	
	public static void markActions(boolean[] vector){
		int i = 0;
		for(Proposition p : propNet.getInputPropositions().values()){
			p.setValue(vector[i]);
			i++;
		}
	}
	
	public static void clearPropnet(){
		markBases(new boolean[basesAmount]);
	}
	*/
	public static boolean readProposition(GGPGameState gamestate, Component p){
		int propIndex;
		boolean propValue;
		try{
		if(p instanceof Constant){
			//System.out.printf("[PropNet] This is constant: %s, value: %b\n",p.toString(),p.getValue());
			return p.getValue();
		}
		if(p instanceof And){  // MULTITHREAD
			for(Component c : p.getInputs())
				if(!readProposition(gamestate, c))
					return false;
			return true;
		}
		if(p instanceof Or){ // MULTITHREAD
			for(Component c : p.getInputs())
				if(readProposition(gamestate, c))
					return true;
			return false;
		}
		if(p instanceof Not){
			return !readProposition(gamestate, p.getSingleInput());
		}
		if (p.equals(propNet.getInitProposition()))
			return p.getValue();
		if (p.getInputs().isEmpty())//(propNet.getInputPropositions().containsValue((Proposition)p))
			return gamestate.getInputValue(propNet.getInputPropositionIndex((Proposition)p));
		if (p.getSingleInput() instanceof Transition)//(propNet.getBasePropositions().containsValue((Proposition)p))
			return gamestate.getBaseValue(propNet.getBasePropositionIndex((Proposition)p));
		propIndex = propNet.getPropIndex((Proposition)p);
		if(gamestate.propEvaluated(propIndex))
			return gamestate.propValue(propIndex);
		else{
			propValue = readProposition(gamestate,p.getSingleInput());
			gamestate.enterPropValue(propIndex, propValue);
			return propValue;
		}
		//return readProposition(gamestate,p.getSingleInput());
		}
		catch(Exception e){
			System.out.println("////ERR\\\\");
			//System.out.println("Message: " + e.getMessage());
			System.out.println("Name: " + p.toString());
			//System.out.println("Value: " + p.getValue());
			if(p instanceof Proposition){
				if(p.getInputs().isEmpty())
					System.out.println("[GameInfo] Err: Proposition considered to be an input.");
				if (p.getSingleInput() instanceof Transition)
					System.out.println("[GameInfo] Err: Proposition considered to be a base.");
			}
			
			
			System.out.println("\\\\ERR////");
			System.exit(0);
		}
		return false;
	}
	
	/*
	private static boolean readBaseProposition(GGPGameState gamestate, Component p){
		if(p instanceof Constant){
			//System.out.printf("[PropNet] This is constant: %s, value: %b\n",p.toString(),p.getValue());
			return p.getValue();
		}
		if(p instanceof And){
			for(Component c : p.getInputs())
				if(!readBaseProposition(gamestate, c))
					return false;
			return true;
		}
		if(p instanceof Or){
			for(Component c : p.getInputs())
				if(readBaseProposition(gamestate, c))
					return true;
			return false;
		}
		if(p instanceof Not){
			return !readBaseProposition(gamestate, p.getSingleInput());
		}
		if (p.equals(propNet.getInitProposition()))
			return p.getValue();
		if (p.getInputs().isEmpty())//(propNet.getInputPropositions().containsValue((Proposition)p))
			return gamestate.getInputValue(propNet.getInputPropositionIndex((Proposition)p));
		if (p.getSingleInput() instanceof Transition)//(propNet.getBasePropositions().containsValue((Proposition)p))
			return gamestate.getBaseValue(propNet.getBasePropositionIndex((Proposition)p));
		return readBaseProposition(gamestate,p.getSingleInput());
	}*/

}
