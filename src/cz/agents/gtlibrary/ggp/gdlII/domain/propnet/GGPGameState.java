package cz.agents.gtlibrary.ggp.gdlII.domain.propnet;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.ggp.base.util.gdl.grammar.GdlConstant;
import org.ggp.base.util.gdl.grammar.GdlRelation;
import org.ggp.base.util.gdl.grammar.GdlTerm;
import org.ggp.base.util.propnet.architecture.components.Proposition;
import org.ggp.base.util.statemachine.Role;

import cz.agents.gtlibrary.iinodes.GGPGameStatePropnetImpl;
import cz.agents.gtlibrary.interfaces.Action;
import cz.agents.gtlibrary.interfaces.GameState;
import cz.agents.gtlibrary.interfaces.Player;
import cz.agents.gtlibrary.interfaces.Sequence;
import cz.agents.gtlibrary.utils.Pair;



public class GGPGameState extends GGPGameStatePropnetImpl{

	private static final long serialVersionUID = 7993011820395567399L;
	
	private List<GGPAction> simultaneousActions; // hash N
	private int currentPlayerIndex; // hash Y
	private int currentGameTreeDepth; // hash Y
	
	private Pair<Integer, Sequence> ISkey;
	private int hashCode = -1;
	
	private boolean[] bases; // hash Y
	private boolean[] inputs; // hash Y
	private boolean[] props;
	private boolean[] propsSatisfied;
	//private boolean[] percepts; // hash Y -> IS Key
	//private
	
	public GGPGameState(GGPGameState gameState) {
		super(gameState);
		//System.out.println("[GameState] New.");
		//if(GGPGameInfo.DEBUG) System.out.println("[Game] Creating new state");
		this.simultaneousActions = new ArrayList<GGPAction>(gameState.simultaneousActions);
		this.currentPlayerIndex = gameState.currentPlayerIndex;
		this.currentGameTreeDepth = gameState.currentGameTreeDepth;
		this.bases = deepCopy(gameState.bases);
		this.inputs = deepCopy(gameState.inputs);
		this.props = deepCopy(gameState.props);
		this.propsSatisfied = deepCopy(gameState.propsSatisfied);
	}
	
	public GGPGameState() {
		super(GGPGameInfo.ALL_PLAYERS, GGPGameInfo.roles);
		this.simultaneousActions = new ArrayList<GGPAction>();
		this.currentPlayerIndex = 0;
		this.currentGameTreeDepth = 0;
		
		//record bases
		this.bases = new boolean[GGPGameInfo.propNet.getNumBases()];
		for(int i = 0; i<bases.length; i++)
			if(GGPGameInfo.propNet.getBaseProposition(i).getSingleInput().getSingleInput().equals(GGPGameInfo.propNet.getInitProposition()))
				bases[i] = true;
		this.inputs = new boolean[GGPGameInfo.propNet.getNumInputs()];
		this.props = new boolean[GGPGameInfo.propNet.getNumProps()];
		this.propsSatisfied = new boolean[GGPGameInfo.propNet.getNumProps()];
		GGPGameInfo.propNet.getInitProposition().setValue(false);
	}
	
	public int getCurrentPlayerIndex(){
		return currentPlayerIndex;
	}

	@Override
	public double getProbabilityOfNatureFor(Action action) {
		return 1.00/(((GGPAction) action).getRandomActionsAmount());
	}

	@Override
	public Pair<Integer, Sequence> getISKeyForPlayerToMove() {
		//System.out.println("[GameState] is.");
		if (ISkey == null){
			ISkey = new Pair<Integer, Sequence>(perceptHistory.get(getPlayerRoleToMove()).hashCode(), getSequenceForPlayerToMove());
		}
		return ISkey;
	}

	@Override
	public Player getPlayerToMove() {
		return players[currentPlayerIndex];
	}
	
	public Role getPlayerRoleToMove() {
		return GGPGameInfo.roles.get(currentPlayerIndex);
	}

	@Override
	public GameState copy() {
		return new GGPGameState(this);
	}

	@Override
	public double[] getUtilities() {
		//System.out.println("[GameState] Getting utilities.");
		if (isGameEnd()){
			return proprewards();
		}
		else return new double[GGPGameInfo.roles.size()];
	}

	@Override
	public boolean isGameEnd() {
		//System.out.println("[GameState] end.");
		boolean end2 = GGPGameInfo.readProposition(this,GGPGameInfo.propNet.getTerminalProposition());
		boolean end = currentGameTreeDepth == GGPGameInfo.maxDepth;
		//if (end)
			//System.out.println("[Game] Terminal state reached.");
		return end || end2;
	}

	@Override
	public boolean isPlayerToMoveNature() {
		return GGPGameInfo.randomPlayerIndex == currentPlayerIndex;
	}


	@Override
	public int hashCode() {
		if (hashCode == -1)
			hashCode = history.hashCode();
		return hashCode;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GGPGameState other = (GGPGameState) obj;
		if (!Arrays.equals(bases, other.bases))
			return false;
		if (currentGameTreeDepth != other.currentGameTreeDepth)
			return false;
		if (currentPlayerIndex != other.currentPlayerIndex)
			return false;
		if (!Arrays.equals(inputs, other.inputs))
			return false;
		if (simultaneousActions == null) {
			if (other.simultaneousActions != null)
				return false;
		} else if (!simultaneousActions.equals(other.simultaneousActions))
			return false;
		if (history == null) {
			if (other.history != null)
				return false;
		} else if (!history.equals(other.history))
			return false;
		return true;
	}

	public void performAction(GGPAction action){
		//System.out.println("[GameState] perform.");
		cleanCache();
		simultaneousActions.add(action);
		if(currentPlayerIndex == GGPGameInfo.ALL_PLAYERS.length-1){
			//if (GGPGameInfo.DEBUG) System.out.println("[Game] End of round.");
			int i;
			// clear
			propsSatisfied = new boolean[GGPGameInfo.propNet.getNumProps()];
			inputs = new boolean[GGPGameInfo.propNet.getNumInputs()];
			//for(i = 0; i<inputs.length;i++)
				//inputs[i] = false;
			
			//add to sequence
			for(GGPAction a : simultaneousActions)
				inputs[a.getInputPropositionIndex()] = true; //new inputs
			
			/*
			if (GGPGameInfo.DEBUG){
				int j = 0;
				System.out.println("[Game] New input props values..");
				for(i = 0; i<inputs.length;i++)
					System.out.printf("%d ", j = (inputs[i]) ? 1 : 0);
				System.out.println();
			}*/
			
			//new percepts
			
			Set<GdlTerm> percepts;
			for(Role r : GGPGameInfo.roles){
				//Proposition[] prop = (Proposition[])GGPGameInfo.propNet.getPerceptsOf(r).toArray();
				percepts = new HashSet<GdlTerm>();
				/*
				for(int threads = 0; threads < Runtime.getRuntime().availableProcessors();threads++){
					Thread t = new Thread(new ito(threads,prop,this,percepts));
					t.start();
					try {
						t.join();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}*/
				
				
				
				
				//if(!GGPGameInfo.boostedPerceptFinding || r.equals(GGPGameInfo.boostedPlayerIndex))
					for(Proposition p : GGPGameInfo.propNet.getPerceptsOf(r)){
						//System.out.printf("[GameState] Testing percept %s for role %s\n",p.getName(),r.getName());
						if(GGPGameInfo.readProposition(this,p)){
							percepts.add(p.getName().get(1));
							//System.out.printf("[Percept of %s] : %s\n",r.toString(),p.getName().get(1).toString());
						}}
				perceptHistory.get(r).add(percepts);
			}
			
			
			//new bases
			boolean[] newBases = new boolean[bases.length];
			for(i = 0; i<bases.length; i++)
				newBases[i] = GGPGameInfo.readProposition(this, GGPGameInfo.propNet.getBaseProposition(i).getSingleInput().getSingleInput());
			bases = newBases;
			/*
			if (GGPGameInfo.DEBUG) {
				int j = 0;
				System.out.println("[Game] New base props values..");
				for(i = 0; i<bases.length; i++)
					System.out.printf("%d ",j = (bases[i]) ? 1 : 0);
				System.out.println();
			}*/
			
			propsSatisfied = new boolean[GGPGameInfo.propNet.getNumProps()];
			
			currentGameTreeDepth++;
			simultaneousActions.clear();
			currentPlayerIndex = 0;
		}
		else{
			currentPlayerIndex++;
		}
	}

	private void cleanCache() {
		ISkey = null;
		hashCode = -1;
	}
	
	private double[] proprewards(){
		//System.out.println("[GameState] rew.");
		Set<Proposition> goals;
		int i = 0;
		double reward;
		double[] rewards = new double[GGPGameInfo.roles.size()];
		for(Role r : GGPGameInfo.roles){
			goals = GGPGameInfo.propNet.getGoalPropositions().get(r);
			for(Proposition p : goals){
				reward = getGoalValue(p);
				if(reward > rewards[i] && GGPGameInfo.readProposition(this,p))
					rewards[i] = reward;
			}
			i++;
		}
		//System.out.println(rewards.toString());
		//System.out.println();
		/*for(double d : rewards)
			System.out.printf("%f ",d);
		System.out.println();*/
		return rewards;
	}
	
    private double getGoalValue(Proposition goalProposition){
    	//System.out.println("[GameState] goal.");
		GdlRelation relation = (GdlRelation) goalProposition.getName();
		GdlConstant constant = (GdlConstant) relation.get(1);
		return Double.parseDouble(constant.toString());
	}
    
    public boolean[] getBases(){
    	return bases;
    }
    
    public boolean getBaseValue(int index){
    	return bases[index];
    }
    
    public boolean getInputValue(int index){
    	return inputs[index];
    }
    
    public int getCurrentTreeDepth(){
    	return currentGameTreeDepth;
    }
    
    public long getPerceptHistoryHash(){
    	return perceptHistory.get(getPlayerRoleToMove()).hashCode();
    }
    
    public String getPrintedPerceptHistory(){
    	return perceptHistory.get(getPlayerRoleToMove()).toString();
    }
    
    public boolean propEvaluated(int index){
    	return propsSatisfied[index];
    }
    
    public boolean propValue(int index){
    	return props[index];
    }
    
    public void enterPropValue(int index, boolean value){
    	props[index] = value;
    	propsSatisfied[index] = true;
    }
    
    private boolean[] deepCopy(boolean[] array){
    	boolean[] newArray = new boolean[array.length];
    	for(int i = 0; i< array.length; i++)
    		newArray[i] = array[i];
    	return newArray;
    }
	
    /*
    private class ito implements Runnable{
    	
    	private int count;
    	private Proposition[] prop;
    	private Set<GdlTerm> percepts;
    	private GGPGameState gs;
    	
    	public ito(int counts, Proposition[] prop, GGPGameState gs, Set<GdlTerm> percepts){
    		this.count = counts;
    		this.prop = prop;
    		this.gs = gs;
    		this.percepts = percepts;
    	}

		@Override
		public void run() {
			for(int i  = count * (int)(prop.length/Runtime.getRuntime().availableProcessors()); i < Math.min(prop.length-1,(int)((prop.length+1)/Runtime.getRuntime().availableProcessors()));i++){
				if(GGPGameInfo.readProposition(gs,prop[i])){
					percepts.add(prop[i].getName().get(1));
				}
			}
		}
    }*/
}
