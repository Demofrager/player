package cz.agents.gtlibrary.ggp.gdlII.domain.prover;

import org.ggp.base.util.gdl.grammar.GdlTerm;

import cz.agents.gtlibrary.iinodes.ActionImpl;
import cz.agents.gtlibrary.interfaces.GameState;
import cz.agents.gtlibrary.interfaces.InformationSet;

public class GGPAction extends ActionImpl{
	


	/**
	 * 
	 */
	private static final long serialVersionUID = 3419793050369474471L;
	
	private int actionsInState = 0;
	protected final GdlTerm contents;
	
	private int hashCode = -1;
	
	public GGPAction(GdlTerm con, InformationSet informationSet, int possibleActions){
		super(informationSet);
		//this.inputPropositionIndex = inputPropositionIndex;
		this.actionsInState = possibleActions;
		this.contents = con;
		
	}
	
	public GdlTerm getTerm(){
		return contents;
	}
	
	public int getRandomActionsAmount(){
		return actionsInState;
	}

	@Override
	public void perform(GameState gameState) {
		((GGPGameState)gameState).performAction(this);
	}

	@Override
	public int hashCode() {
		if (hashCode == -1) {
			final int prime = 31;
			int result = 1;
			
			result = prime * result + ((informationSet == null) ? 0 : informationSet.hashCode());
			result = prime * result + actionsInState;
			if(contents!=null)result = prime * result + contents.hashCode();
			hashCode = result;
		}
		return hashCode;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		GGPAction other = (GGPAction) obj;
		if (actionsInState != other.actionsInState)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return contents==null? "DummyA" : contents.toString();
	}

}
