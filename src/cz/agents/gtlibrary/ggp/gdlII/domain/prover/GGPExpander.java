package cz.agents.gtlibrary.ggp.gdlII.domain.prover;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.ggp.base.util.gdl.grammar.GdlSentence;

import cz.agents.gtlibrary.iinodes.ExpanderImpl;
import cz.agents.gtlibrary.interfaces.Action;
import cz.agents.gtlibrary.interfaces.AlgorithmConfig;
import cz.agents.gtlibrary.interfaces.GameState;
import cz.agents.gtlibrary.interfaces.InformationSet;

public class GGPExpander<I extends InformationSet> extends ExpanderImpl<I> {


	/**
	 * 
	 */
	private static final long serialVersionUID = 6210108003017566210L;

	public GGPExpander(AlgorithmConfig<I> algConfig) {
		super(algConfig);
	}

	@Override
	public List<Action> getActions(GameState gameState) {
		return getActions((GGPGameState)gameState);
	}
	
	private List<Action> getActions(GGPGameState gameState){
		int possibleActions;
		List<Action> actions = new ArrayList<Action>();
		
		if(GGPGameInfo.isOnePlayerGame && gameState.getCurrentPlayerIndex()==0){
			actions.add(new GGPAction(null,getAlgorithmConfig().getInformationSetFor(gameState),1));
			return actions;
		}
		
		Set<GdlSentence> results = GGPGameInfo.prover.askAll(GGPGameInfo.getLegalQuery(gameState.getPlayerRoleToMove()), gameState.getContents());
		possibleActions = results.size();
		if (results.size() == 0)
		{
			System.out.println("[Expander] Err: no possible action.");
		}
		
		for (GdlSentence result : results){
			actions.add(new GGPAction(result.get(1),getAlgorithmConfig().getInformationSetFor(gameState),possibleActions));
		}
		
		//if (actions.size()==0)
			//System.out.printf("[Expander] Err: no possible action for player %s!\n",gameState.getPlayerRoleToMove().getName());
		return actions;
	}

}
