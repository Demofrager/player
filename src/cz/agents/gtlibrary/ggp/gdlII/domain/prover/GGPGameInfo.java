package cz.agents.gtlibrary.ggp.gdlII.domain.prover;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
//import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.ggp.base.util.gdl.grammar.Gdl;
import org.ggp.base.util.gdl.grammar.GdlConstant;
import org.ggp.base.util.gdl.grammar.GdlPool;
import org.ggp.base.util.gdl.grammar.GdlProposition;
import org.ggp.base.util.gdl.grammar.GdlRelation;
import org.ggp.base.util.gdl.grammar.GdlSentence;
import org.ggp.base.util.gdl.grammar.GdlTerm;
import org.ggp.base.util.gdl.grammar.GdlVariable;
import org.ggp.base.util.prover.Prover;
import org.ggp.base.util.prover.aima.AimaProver;
import org.ggp.base.util.statemachine.Role;

import com.rits.cloning.Cloner;

import cz.agents.gtlibrary.iinodes.PlayerImpl;
import cz.agents.gtlibrary.interfaces.GameInfo;
import cz.agents.gtlibrary.interfaces.Player;

public class GGPGameInfo implements GameInfo{
	
	public static final boolean DEBUG = false;
	
	public static Prover prover;
    public static List<Role> roles;
    public static int randomPlayerIndex;
	
	public static Player[] ALL_PLAYERS;
	
	public static boolean isOnePlayerGame;
	
	public static int maxDepth;
	
	public static PrintWriter writer;
	public static Cloner cloner;
	
	public GGPGameInfo(List<Gdl> description) throws FileNotFoundException, UnsupportedEncodingException{
		
		prover = new AimaProver(description);
		roles = Role.computeRoles(description);
	
		GGPGameInfo.maxDepth = Integer.MAX_VALUE;
		
		relistRoles();
		
		//randomPlayerIndex=-1 armin;
		System.out.println("<>randomPlayerIndex: " + randomPlayerIndex );
		System.out.println("<>roles.size(): " + roles.size());
		System.out.println("<>roles : " + roles );
		
		
		if((randomPlayerIndex!=-1 && roles.size()==2) || roles.size()==1){
			ALL_PLAYERS = new Player[roles.size()+1];
			for(int i = 0; i < roles.size()+1; i++){
				ALL_PLAYERS[i] = new PlayerImpl(i);
			}
			isOnePlayerGame = true;
			
			if(randomPlayerIndex!=-1)
				randomPlayerIndex++;
		}
		
		else{
			ALL_PLAYERS = new Player[roles.size()];
			for(int i = 0; i < roles.size(); i++){
				//System.out.println(roles.get(i).toString());
				ALL_PLAYERS[i] = new PlayerImpl(i);
			}
			isOnePlayerGame=false;
		}
		
		System.out.printf("[GameInfo] Random player index: %d.\n",randomPlayerIndex);
		
		//findRandom();
		//randomPlayerIndex=-1; // NATVRDO NENI RANDOM
		//maxDepth = 2;
		cloner = new Cloner();
	}
	
	private void relistRoles(){
		findRandom();
		if(randomPlayerIndex != -1){
			Collections.swap(roles, randomPlayerIndex, roles.size()-1);
			randomPlayerIndex = roles.size()-1;
		}
	}
	
	public static Player getPlayerByRole(GdlConstant role){
		for(int i = 0; i<roles.size(); i++)
			if(roles.get(i).getName().equals(role))
				return isOnePlayerGame? ALL_PLAYERS[i+1] : ALL_PLAYERS[i];
		return null;
	}
	
	public static int getPlayerIndexByRole(GdlConstant role){
		for(int i = 0; i<roles.size(); i++)
			if(roles.get(i).getName().equals(role))
				return isOnePlayerGame? i+1 : i;
		return -1;
	}
	
	public static int getRoleIndexByRole(GdlConstant role){
		for(int i = 0; i<roles.size(); i++)
			if(roles.get(i).getName().equals(role))
				return i;
		return -1;
	}
	
	private void findRandom(){
		int i = 0;
		for(Role r : roles){
			if(r.getName().getValue().toLowerCase().equals("random")){
				randomPlayerIndex = i;
				return;
			}
			i++;
		}
		randomPlayerIndex = -1; // random player not present
	}
		

	@Override
	public double getMaxUtility() {
		return 100;
	}

	@Override
	public Player getFirstPlayerToMove() {
		return ALL_PLAYERS[0];
	}

	@Override
	public Player getOpponent(Player player) {
		return player.equals(ALL_PLAYERS[0]) ? ALL_PLAYERS[1] : ALL_PLAYERS[0];
		//return null;
	}

	@Override
	public String getInfo() {
		return "GGP Game";
	}

	@Override
	public int getMaxDepth() {
		return maxDepth;
	}

	@Override
	public Player[] getAllPlayers() {
		return ALL_PLAYERS;
	}
	
	private final static GdlConstant TRUE = GdlPool.getConstant("true");

	/*
	public static List<Move> toMoves(Set<GdlSentence> results)
	{
		List<Move> moves = new ArrayList<Move>();
		for (GdlSentence result : results)
		{
			moves.add(new Move(result.get(1)));
		}

		return moves;
	}*/

	public static List<Role> toRoles(List<GdlSentence> results)
	{
		List<Role> roles = new ArrayList<Role>();
		for (GdlSentence result : results)
		{
			GdlConstant name = (GdlConstant) result.get(0);
			roles.add(new Role(name));
		}

		return roles;
	}

	public static Set<GdlSentence> toState(Set<GdlSentence> results)
	{
		Set<GdlSentence> trues = new HashSet<GdlSentence>();
		for (GdlSentence result : results)
		{
			trues.add(GdlPool.getRelation(TRUE, new GdlTerm[] { result.get(0) }));
		}
		return trues;
	}
	
	
	private final static GdlConstant DOES = GdlPool.getConstant("does");
	private final static GdlConstant GOAL = GdlPool.getConstant("goal");
	private final static GdlRelation INIT_QUERY = GdlPool.getRelation(GdlPool.getConstant("init"), new GdlTerm[] { GdlPool.getVariable("?x") });
	private final static GdlConstant LEGAL = GdlPool.getConstant("legal");
	private final static GdlRelation NEXT_QUERY = GdlPool.getRelation(GdlPool.getConstant("next"), new GdlTerm[] { GdlPool.getVariable("?x") });
	private final static GdlRelation ROLE_QUERY = GdlPool.getRelation(GdlPool.getConstant("role"), new GdlTerm[] { GdlPool.getVariable("?x") });
	private final static GdlProposition TERMINAL_QUERY = GdlPool.getProposition(GdlPool.getConstant("terminal"));
	private final static GdlVariable VARIABLE = GdlPool.getVariable("?x");
	private final static GdlConstant SEES = GdlPool.getConstant("SEES");


	public static Set<GdlSentence> getContext(Set<GdlSentence> context, List<GGPAction> moves)
	{
		//Set<GdlSentence> context = new HashSet<GdlSentence>(state.getContents());
		for (int i = 0; i < roles.size(); i++)
		{
			context.add(toDoes(roles.get(i), moves.get(i).getTerm()));
		}
		return context;
	}

	public static GdlRelation getGoalQuery(Role role)
	{
		return GdlPool.getRelation(GOAL, new GdlTerm[] { role.getName(), VARIABLE });
	}

	public static GdlRelation getInitQuery()
	{
		return INIT_QUERY;
	}

	public static GdlRelation getLegalQuery(Role role)
	{
		return GdlPool.getRelation(LEGAL, new GdlTerm[] { role.getName(), VARIABLE });
	}
	
	public static GdlRelation getSeesQuery(Role role)
	{
		return GdlPool.getRelation(SEES, new GdlTerm[] { role.getName(), VARIABLE });
	}

	public static GdlRelation getNextQuery()
	{
		return NEXT_QUERY;
	}

	public static GdlRelation getRoleQuery()
	{
		return ROLE_QUERY;
	}

	public static GdlProposition getTerminalQuery()
	{
		return TERMINAL_QUERY;
	}

	public static GdlRelation toDoes(Role role, GdlTerm move)
	{
		return GdlPool.getRelation(DOES, new GdlTerm[] { role.getName(), move });
	}

}
