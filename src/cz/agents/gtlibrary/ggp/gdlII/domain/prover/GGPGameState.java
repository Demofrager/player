package cz.agents.gtlibrary.ggp.gdlII.domain.prover;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.ggp.base.util.gdl.grammar.GdlConstant;
import org.ggp.base.util.gdl.grammar.GdlRelation;
import org.ggp.base.util.gdl.grammar.GdlSentence;
import org.ggp.base.util.gdl.grammar.GdlTerm;
import org.ggp.base.util.logging.GamerLogger;
import org.ggp.base.util.statemachine.Role;

import cz.agents.gtlibrary.iinodes.GGPGameStateProverImpl;
import cz.agents.gtlibrary.interfaces.Action;
import cz.agents.gtlibrary.interfaces.GameState;
import cz.agents.gtlibrary.interfaces.Player;
import cz.agents.gtlibrary.interfaces.Sequence;
import cz.agents.gtlibrary.utils.Pair;



public class GGPGameState extends GGPGameStateProverImpl{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3828449315133653677L;
	
	private List<GGPAction> simultaneousActions; // hash N
	private int currentPlayerIndex; // hash Y
	private int currentGameTreeDepth; // hash Y
	
	private Pair<Integer, Sequence> ISkey;
	private int hashCode = -1;
	
	private Set<GdlSentence> contents;
	
	public GGPGameState(GGPGameState gameState) {
		super(gameState);
		this.simultaneousActions = new ArrayList<GGPAction>(gameState.simultaneousActions);
		this.currentPlayerIndex = gameState.currentPlayerIndex;
		this.currentGameTreeDepth = gameState.currentGameTreeDepth;
		this.contents = getContentsDeepCopy(gameState.contents);
	}
	
	public GGPGameState() {
		super(GGPGameInfo.ALL_PLAYERS, GGPGameInfo.roles);
		this.simultaneousActions = new ArrayList<GGPAction>();
		this.currentPlayerIndex = 0;
		this.currentGameTreeDepth = 0;
		
		System.out.println("[GameState] Computing initial state.");
		this.contents = GGPGameInfo.toState(GGPGameInfo.prover.askAll(GGPGameInfo.getInitQuery(), new HashSet<GdlSentence>()));
		System.out.println("[GameState] Initial state computed.");
	}
	
	public int getCurrentPlayerIndex(){
		return currentPlayerIndex;
	}

	@Override
	public double getProbabilityOfNatureFor(Action action) {
		return 1.00/(((GGPAction) action).getRandomActionsAmount());
	}

	@Override
	public Pair<Integer, Sequence> getISKeyForPlayerToMove() {
		//System.out.println("[GameState] is.");
		if (ISkey == null){
			if(GGPGameInfo.isOnePlayerGame && currentPlayerIndex==0)
				ISkey = new Pair<Integer, Sequence>(0, getSequenceForPlayerToMove());
			else
				ISkey = new Pair<Integer, Sequence>(perceptHistory.get(getPlayerRoleToMove()).hashCode(), getSequenceForPlayerToMove());
		}
		return ISkey;
	}

	@Override
	public Player getPlayerToMove() {
		return players[currentPlayerIndex];
	}
	
	public Role getPlayerRoleToMove() {
		return GGPGameInfo.isOnePlayerGame? GGPGameInfo.roles.get(currentPlayerIndex-1) : GGPGameInfo.roles.get(currentPlayerIndex);
	}

	@Override
	public GameState copy() {
		return new GGPGameState(this);
	}

	@Override
	public double[] getUtilities() {
		//System.out.println("[GameState] Getting utilities.");
		if (isGameEnd()){
			return proprewards();
		}
		else return new double[GGPGameInfo.ALL_PLAYERS.length];
	}

	@Override
	public boolean isGameEnd() {
		//System.out.println("[GameState] end.");
		boolean end2 = GGPGameInfo.prover.prove(GGPGameInfo.getTerminalQuery(), contents);
		boolean end = currentGameTreeDepth == GGPGameInfo.maxDepth;
		//if (end)
			//System.out.println("[Game] Terminal state reached.");
		return end || end2;
	}

	@Override
	public boolean isPlayerToMoveNature() {
		return GGPGameInfo.randomPlayerIndex == currentPlayerIndex;
	}


	@Override
	public int hashCode() {
		if (hashCode == -1)
			hashCode = history.hashCode();
		return hashCode;	
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GGPGameState other = (GGPGameState) obj;
		if (contents == null) {
			if (other.contents != null)
				return false;
		} else if (!contents.equals(other.contents))
			return false;
		if (currentGameTreeDepth != other.currentGameTreeDepth)
			return false;
		if (currentPlayerIndex != other.currentPlayerIndex)
			return false;
		if (simultaneousActions == null) {
			if (other.simultaneousActions != null)
				return false;
		} else if (!simultaneousActions.equals(other.simultaneousActions))
			return false;
		if (history == null) {
			if (other.history != null)
				return false;
		} else if (!history.equals(other.history))
			return false;
		
		if (perceptHistory == null) {
			if (other.perceptHistory != null)
				return false;
		} else if (!perceptHistory.equals(other.perceptHistory))
			return false;
		return true;
	}

	public void performAction(GGPAction action){
		//System.out.println("[GameState] perform.");
		cleanCache();
		if (!GGPGameInfo.isOnePlayerGame || currentPlayerIndex!=0)
			simultaneousActions.add(action);
		if(currentPlayerIndex == GGPGameInfo.ALL_PLAYERS.length-1){	
			
			Set<GdlSentence> nextContext = GGPGameInfo.getContext(contents, simultaneousActions);
			
			Set<GdlSentence> results2 = null;
			Set<GdlTerm> percepts;
			for(Role r : GGPGameInfo.roles){
				percepts = new HashSet<GdlTerm>();
				results2 = GGPGameInfo.prover.askAll(GGPGameInfo.getSeesQuery(r), nextContext);
				for (GdlSentence result : results2){
					percepts.add(result.get(1));
					}
				//System.out.println(percepts.size());
				perceptHistory.get(r).add(percepts);
			}
			
			
			Set<GdlSentence> results = GGPGameInfo.prover.askAll(GGPGameInfo.getNextQuery(), nextContext);

			for (GdlSentence sentence : results)
			{
				if (!sentence.isGround())
				{
					//throw new TransitionDefinitionException(state, moves);
					System.out.println("[GameState] Err: sentence not ground.");
				}
			}

			contents = GGPGameInfo.toState(results);
						
			currentGameTreeDepth++;
			simultaneousActions.clear();
			currentPlayerIndex = 0;
		}
		else{
			currentPlayerIndex++;
		}
	}

	private void cleanCache() {
		ISkey = null;
		hashCode = -1;
	}
	
	private double[] proprewards(){
		int i = 0;
		double[] rewards = new double[GGPGameInfo.ALL_PLAYERS.length];
		if(GGPGameInfo.isOnePlayerGame)
			i=1;
		for(Role r : GGPGameInfo.roles){
			rewards[i] = getGoal(r);
			i++;
		}
		if(GGPGameInfo.isOnePlayerGame)
			rewards[0] = - rewards[1];
		return rewards;
	}
    
    public int getCurrentTreeDepth(){
    	return currentGameTreeDepth;
    }
    
    public long getPerceptHistoryHash(){
    	return perceptHistory.get(getPlayerRoleToMove()).hashCode();
    }
    
    public String getPrintedPerceptHistory(){
    	return perceptHistory.get(getPlayerRoleToMove()).toString();
    }
    
    public int getGoal(Role role) //throws GoalDefinitionException
	{
		Set<GdlSentence> results = GGPGameInfo.prover.askAll(GGPGameInfo.getGoalQuery(role), contents);

		if (results.size() != 1)
		{
		    GamerLogger.logError("StateMachine", "Got goal results of size: " + results.size() + " when expecting size one.");
			//throw new GoalDefinitionException(state, role);
		}

		try
		{
			GdlRelation relation = (GdlRelation) results.iterator().next();
			GdlConstant constant = (GdlConstant) relation.get(1);

			return Integer.parseInt(constant.toString());
		}
		catch (Exception e)
		{
			System.out.println("[GameState] Err: getGoal.");
			//throw new GoalDefinitionException(state, role);
		}
		return -1;
	}
    
    public Set<GdlSentence> getContents(){
    	return contents;
    }
    
    private Set<GdlSentence> getContentsDeepCopy(Set<GdlSentence> contents){
    	Set<GdlSentence> copy = new HashSet<GdlSentence>();
    	for(GdlSentence sentence : contents){
    		copy.add(GGPGameInfo.cloner.deepClone(sentence));
    	}
    	return copy;
    }
    
}
