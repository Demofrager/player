package cz.agents.gtlibrary.ggp.gdlII.player;
//Armin Chitizadeh: This is perfectly working Single player for GGP-II 
// This is an extension over the previous ShoDanArminF2 . in this version
// It works for two player games too.
import java.util.*;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


import org.ggp.base.player.gamer.Gamer;
import org.ggp.base.player.gamer.exception.AbortingException;
import org.ggp.base.player.gamer.exception.GamePreviewException;
import org.ggp.base.player.gamer.exception.MetaGamingException;
import org.ggp.base.player.gamer.exception.MoveSelectionException;
import org.ggp.base.player.gamer.exception.StoppingException;
import org.ggp.base.util.game.Game;
import org.ggp.base.util.gdl.grammar.GdlTerm;
import org.ggp.base.util.match.Match;
import org.ggp.base.util.statemachine.Role;

import cz.agents.gtlibrary.algorithms.mcts.MCTSConfig;
import cz.agents.gtlibrary.algorithms.mcts.MCTSInformationSet;
import cz.agents.gtlibrary.algorithms.mcts.MCTSRunner;
import cz.agents.gtlibrary.algorithms.mcts.Simulator;
import cz.agents.gtlibrary.algorithms.mcts.distribution.FrequenceDistribution;
import cz.agents.gtlibrary.algorithms.mcts.nodes.ChanceNode;
import cz.agents.gtlibrary.algorithms.mcts.nodes.InnerNode;
import cz.agents.gtlibrary.algorithms.mcts.nodes.LeafNode;
import cz.agents.gtlibrary.algorithms.mcts.nodes.Node;
import cz.agents.gtlibrary.algorithms.mcts.selectstrat.Exp3BackPropFactory;
import cz.agents.gtlibrary.ggp.gdlII.domain.prover.GGPAction;
import cz.agents.gtlibrary.ggp.gdlII.domain.prover.GGPExpander;
import cz.agents.gtlibrary.ggp.gdlII.domain.prover.GGPGameInfo;
import cz.agents.gtlibrary.ggp.gdlII.domain.prover.GGPGameState;
import cz.agents.gtlibrary.iinodes.ArrayListSequenceImpl;
import cz.agents.gtlibrary.interfaces.Action;
import cz.agents.gtlibrary.interfaces.GameState;
import cz.agents.gtlibrary.interfaces.Player;
import cz.agents.gtlibrary.interfaces.Sequence;
import cz.agents.gtlibrary.strategy.Strategy;
import cz.agents.gtlibrary.strategy.UniformStrategyForMissingSequences;
import cz.agents.gtlibrary.utils.BeliefISWrapper;
import cz.agents.gtlibrary.utils.LevelPerceptionWrapper;
import cz.agents.gtlibrary.utils.InformationSetClassArmin;
import cz.agents.gtlibrary.utils.MathThing;

public class ShoDanArminF2TP2Nash extends Gamer{
	
    private static final int MCTS_ITERATIONS_PER_CALL = (int)10;
    private static final double gamma = 0.05;
	
    private GGPGameInfo gameInfo;
    private boolean isPlayable;
    private GGPGameState rootState;
    private GGPExpander<MCTSInformationSet> expander;
    
    private int gameTreeDepth;
    private Player myself;
    private Role myselfRole;
    private int myselfIndex;
    
    private ArrayList<InnerNode> currentIS;
    private ArrayList<Double> belief;
    private Sequence actionsPlayed;
    
    private MCTSConfig ggpMCTSConfig;
    private MCTSRunner runner;
    
    private Strategy strategy;
    
    private int arminCounter = 0;
    
    private Random generator;
    private String latexDraw;
    private String[] coloursDraw = {"blue", "red", "green", "orange", "violet", "bluegreen"};
    private String latexDrawForest;
    private String latexDrawInformationSets;
    private Boolean iWantToPlay = null;
    private ArrayList<InformationSetClassArmin> informationS = null;
    private List<Role> allRoles = null;
    
    
    //for computing next belief
    double beliefSum;
    int nSum;
    // The key is of the form of {Level, HashCode, SequenceOfSelfMove}
    HashMap<List<Integer>, List<InnerNode>> InformationSetArmin;
    // The key is similar to the one above but Integer holds the information set value
    HashMap<List<Integer>, Integer> InformationSetValue;
    // The key is similar to the one above but integers holds the total value of random choices
    // by random player for all the states combined, "added up"
    HashMap<List<Integer>, Integer> InformationSetRandomChoices;
    
	@Override
	public void metaGame(long timeout) throws MetaGamingException {
		gameInfo = null;
		try {
			gameInfo = new GGPGameInfo(getMatch().getGame().getRules());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		
		System.out.println("THIS IS ShoDanRokul.java");
		System.out.println("[Armin F2] Setting roles.");
		
		myself = GGPGameInfo.getPlayerByRole(getRoleName()); //<>Get role name is related to parsing GDL
		myselfIndex = GGPGameInfo.getRoleIndexByRole(getRoleName());
        
		System.out.println(" ROLES: " + GGPGameInfo.roles);

				
        myselfRole = GGPGameInfo.roles.get(myselfIndex);
        allRoles = GGPGameInfo.roles;
        
		System.out.println("myself:" + myself.toString() + " | ID: " + myself.getId() + " myselfIndex: " + myselfIndex + " myselfRole: " + myselfRole);

        
		gameTreeDepth = 0;
		
		if(myself == null || myselfRole==null)
			System.out.println("[Armin F] Err: Couldn't instantiate roles.");
		
        if(GGPGameInfo.ALL_PLAYERS.length <= 3) // <>The player is limited to two players or two players and a random player
        	if(GGPGameInfo.randomPlayerIndex == -1 && GGPGameInfo.ALL_PLAYERS.length == 3)
        		isPlayable = false; //<>This is where there are 3 players and none is random
        	else isPlayable = true;
		else
			isPlayable = false;
        
        System.out.println("[Armin F] Setting MCTS.");
        
        rootState = new GGPGameState();
        ggpMCTSConfig = new MCTSConfig(new Simulator(), new Exp3BackPropFactory(0,gameInfo.getMaxUtility(),gamma), new UniformStrategyForMissingSequences.Factory(), null);
        expander = new GGPExpander<MCTSInformationSet> (ggpMCTSConfig);
		runner = new MCTSRunner(ggpMCTSConfig, rootState, expander);
		
		System.out.println("[Armin F] Initializing MCTS tree.");
		
		runner.runMCTS(0, myself); // this creates a rootstate 
		
		
		
		currentIS = new ArrayList<InnerNode>();
		belief = new ArrayList<Double>();
		actionsPlayed = new ArrayListSequenceImpl(myself);
		generator = new Random();
		
		belief.add((double)1.00);
		//<> InnerNodes are same as states but have more information
		currentIS.add(runner.getRootNode()); //<>currentIS is a list of InnerNodes, 
		
		
		InformationSetArmin = new HashMap<List<Integer>, List<InnerNode>>();
		InformationSetValue = new HashMap<List<Integer> , Integer>();
		InformationSetRandomChoices = new HashMap<List<Integer> , Integer>();
		informationS = new ArrayList<InformationSetClassArmin>(); //??May need to be changed
		//initializing the latexDraw
		//latexDraw = "[\n";
		InnerNode initNode = runner.getRootNode();
		
		System.out.println("line1");
		
		SearchTheGameTree(initNode); //<> Just print out the whole tree, and populate the InformationSet
		
		System.out.println("Does it come here?");
		
		double[] tempFirstNodeValue = searchAndPopulateInnerNodesValues(initNode);
        for ( int i=0 ; i<tempFirstNodeValue.length ; i++){
    		System.out.println("theFirstNodevalue:::::::: " + tempFirstNodeValue[i]);        	
        }
        
        //<> Initialising the Latex printing
        latexDrawForest = "";
        latexDrawInformationSets = "";
        

        //checkInformationSetValue(InformationSetArmin);
        populateInformationSetValues(InformationSetArmin);
        System.out.println("It came here!!!");
        printTree(initNode);
        printInformationSet(InformationSetArmin);
        
        //checkInformationSetValue(InformationSetArmin);
        //populateInformationSetValues(InformationSetArmin);
        
        //Printing nodes values in information sets
        System.out.println("ALL///////////");
        for (List<InnerNode> listInnerN : InformationSetArmin.values()){
        	System.out.println("-Information Set:");
        	for (InnerNode innerN : listInnerN){
        		System.out.println("\tvalue:"+ innerN.getNodeValue() + " History:"+innerN.getGameState().getHistory());
        		System.out.print("\t\tThe Perception history: " + ((GGPGameState)innerN.getGameState()).getPerceptHistory().get(allRoles.get(innerN.getPlayerToMove().getId())).toString());
        		System.out.println(" The Player To move is: " + innerN.getPlayerToMove());
        		//int i = ((GGPGameState)innerN.getGameState()).getPerceptHistory().get(allRoles.get(innerN.getPlayerToMove().getId()));
        	}
        }
        System.out.println("///////////");
        System.out.println("CLEAN///////////");
        for (List<InnerNode> listInnerN : InformationSetArmin.values()){
        	System.out.println("-Information Set:");
        	int seqLen = -1;
        	Boolean shouldItBreak = false;
        	for ( Sequence pastMoves: listInnerN.get(0).getGameState().getHistory().getSequencesOfPlayers().values()){
        		if(seqLen==-1){
        			seqLen = pastMoves.size();
        		}else{
        			if(seqLen != pastMoves.size()) shouldItBreak= true;
        		}
        	}
        	if(shouldItBreak) continue;
        	for (InnerNode innerN : listInnerN){
        		System.out.println("\tvalue:"+ innerN.getNodeValue() + " History:"+innerN.getGameState().getHistory());
        	}
        }
        System.out.println("///////////");
        ///////
        
        
        
        System.out.println("THE LATEX TREE FORST :\n" + this.latexDrawForest);
        System.out.println(this.latexDrawInformationSets);
		giveActionToInformationSet(informationS);
        
        
		//System.out.println("//////////////////////////////");
		//System.out.println(latexDraw);
		//System.out.println("//////////////////////////////");
		
		System.out.println(">><<>><<sizeOftheInformationSetKey: " + InformationSetArmin.keySet().size());
		for (List<Integer> name: InformationSetArmin.keySet()){

            System.out.println("The Key are: [" + name.get(0) + ", " + name.get(1) + ", " + name.get(2) + "]");
            for (Node n : InformationSetArmin.get(name)){
            	System.out.println("\t" + n.getGameState().isGameEnd() + "  " + n.getGameState().getHistory().toString());
            }

		}
		
		iWantToPlay = null;
        System.out.println("[Armin F] Ready.");
	}

	@Override
	public GdlTerm selectMove(long timeout) throws MoveSelectionException {
		GGPAction action = null;
		GdlTerm move;
		int iteration = 0;
		long startTime;
		long oneRoundTime = 1;
	    String inputA;
	    boolean solutionFound = false;
		
		System.out.println("[Armin F] Next Round!");
		System.out.printf("\t   Current percept history: %s and the size is:%d \n",getMatch().getPerceptHistory().toString(),  getMatch().getPerceptHistory().size());
		System.out.println("<----INFORMATION ON Information Sets---->");
		if (getMatch().getPerceptHistory().size() > 0)	System.out.println("latest: " + getMatch().getPerceptHistory().get(getMatch().getPerceptHistory().size() -1 ));
		//System.out.println("latest Perception: " + getMatch().getPerceptHistory().get(0).toString());
		System.out.println("my Moves: " + actionsPlayed.toString());
		System.out.printf("<>\t  <> IS size before updateCurrentState(): %d \n",currentIS.size() );
		// This is just testing, does nothing
		if (getMatch().getPerceptHistory().size() > 0){
			for(InformationSetClassArmin is : informationS){
				boolean perceptionEqu = getMatch().getPerceptHistory().hashCode() == is.getPerception();
				boolean sequenceOfSelfMoveEqu = actionsPlayed.hashCode() == is.getSequenceOfSelfMoveHash();
				if (perceptionEqu && sequenceOfSelfMoveEqu){
					System.out.println("There is similar information Set");
					if (is.getChosenAction() != null){
						System.out.println("is.getChosenAciton: " + is.getChosenAction().toString());
					}
				}
			}
		}
		
		
		
		
		//<>This part I try to run, I don't think this part does anything
		BeliefISWrapper biswrapper = updateCurrentStatesFunction( currentIS, gameTreeDepth, getMatch(), actionsPlayed, myselfRole, myself, belief);
		currentIS = biswrapper.getInformationSet();
		belief = biswrapper.getBelief();

		
		
		
		
		
		if (iWantToPlay == null){
			BufferedReader readerOne = new BufferedReader(new InputStreamReader(System.in));
		    System.out.print("Do you want to play? [yes | no] ");
			try {
				inputA = readerOne.readLine();
				// This part just tries to find children that are done by  "action" just 
				if (inputA.toLowerCase().replaceAll("\\p{P}","").replace(" " , "").equals("yes")){
					iWantToPlay = true;
				}else if(inputA.toLowerCase().replaceAll("\\p{P}","").replace(" " , "").equals("no")){
					iWantToPlay = false;
				}else{
					System.out.println("sorry I don't understand what you siad, I assume you said no");
					iWantToPlay = false;
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		if (iWantToPlay == false){
			if (getMatch().getPerceptHistory().size() > 0){
				for(InformationSetClassArmin is : informationS){
					boolean perceptionEqu = getMatch().getPerceptHistory().hashCode() == is.getPerception();
					boolean sequenceOfSelfMoveEqu = actionsPlayed.hashCode() == is.getSequenceOfSelfMoveHash();
					if (perceptionEqu && sequenceOfSelfMoveEqu && is.getChosenAction() != null){
						System.out.println("is.getChosenAciton: " + is.getChosenAction().toString());
						action = (GGPAction)is.getChosenAction();
						solutionFound = true;
					}
				}
			}
			
			
			
			if (!solutionFound)	action =(GGPAction)currentIS.get(0).getActions().get(0);
					
	
		}else if(iWantToPlay == true){
			
		
		
		
		//<>
		
		
		
		//currentIS = updateCurrentStates();
			System.out.printf("\t   IS after function call size: %d \n",currentIS.size());
		
		
			System.out.println("<>>>States in Current information sets are: ");
			for (InnerNode inNode : currentIS){
				System.out.println( "\t" + inNode.getGameState().getHistory());
				
			}
		
			System.out.println("legal actions are: ");
			for (Action a1 : currentIS.get(0).getActions()){
				System.out.println("\t" + a1.toString());
			}
		
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
			System.out.print("Please enter your move? ");
			
			try {
				inputA = reader.readLine();
				System.out.print("I HAVE :" +inputA );
				// This part just tries to find children that are done by  "action" just 
				for ( Action a2 : currentIS.get(0).getActions() ){
					action = (GGPAction)a2;
					System.out.println("Comparing:");
					System.out.print(a2.toString().toLowerCase().replaceAll("\\p{P}","").replace(" " , ""));
					System.out.println(inputA.toLowerCase().replaceAll("\\p{P}","").replace(" " , ""));
							
					if ( a2.toString().toLowerCase().replaceAll("\\p{P}","").replace(" " , "").equals(inputA.toLowerCase().replaceAll("\\p{P}","").replace(" " , ""))){
						System.out.println("<<< You chose " + a2.toString() + " >>>");
						break;
					}
				}
			//
				if (action == null){
					action =(GGPAction)currentIS.get(0).getActions().get(0);
					System.out.println(">>> You DID Not CHOSE <<<");
				}
			
			} catch (IOException e) {
				action = (GGPAction)currentIS.get(0).getActions().get(0);
				e.printStackTrace();
			
			}
		
		}
		
		
		/*
		if(!isPlayable) 
			action = (GGPAction)currentIS.get(0).getActions().get(0);
		else{
			
			
			
			
			//<>Calculation for choosing the move happens here
			System.out.println("[Armin F] Running MCts.");
			startTime = timeout - System.currentTimeMillis();
			for(int i = 0; i<5000; i++){
			//while((timeout - System.currentTimeMillis()) > (2*oneRoundTime+1000)){
			//<>COMMENTED OUT BY ARMIN	System.out.println(iteration*MCTS_ITERATIONS_PER_CALL);
				runner.setRootNode(selectNode());
				runner.runMCTS(MCTS_ITERATIONS_PER_CALL, myself);
				//if(iteration==0)
					oneRoundTime = (startTime - (timeout - System.currentTimeMillis()))/(iteration+1);
				iteration++;
			}
			System.out.printf("[Armin F] Iters: %d. \n",iteration*MCTS_ITERATIONS_PER_CALL);
			// create the strategy
			System.out.println("[Armin F] Computing strategy.");
			strategy = runner.runMCTS(0, myself, new FrequenceDistribution());
		
			// select action
			System.out.println("[Armin F] Selecting action.");
			action = selectAction();
		}
		*/
		//<> --------------
		
		move = action.getTerm();//GGPGameInfo.propNet.getInputProposition(action.getInputPropositionIndex()).getName().get(1);
		System.out.printf("\t   Playing: %s.\n",move.toString());
		gameTreeDepth++;
		actionsPlayed.addLast(action);
		return move;
	}
	
	
	
	
	
	// returns Node with distribution of believes in current IS
	private InnerNode selectNode(){
		double counter = (double) 0;
		double prob = generator.nextDouble();
		for(int i = 0; i< belief.size(); i++){
				counter+=belief.get(i);
				if(counter > prob)
					return currentIS.get(i);
			}
		System.out.println("[Armin F] Err: No node selected.");
		return null;
	}
	
	private GGPAction selectAction(){
		double counter = (double) 0;
		System.out.println(strategy);
		double prob = generator.nextDouble();
		for(Sequence seq : strategy.keySet())
			if(seq.size()==1){
				counter+=strategy.get(seq);
				if(counter > prob){
					return (GGPAction)seq.getFirst();
				}
			}	
		System.out.println("[Armin F] Err: No action selected.");
		return null;
	}
	
	// UZ OK
	private double getOpponentRealization(InnerNode parent, Action parentAction){
		
		if(parent==null)
			return 1.00;
		
		int playerIndex = ((GGPGameState)parent.getGameState()).getCurrentPlayerIndex();		
		double sum = 0.0;
		double parentSum = 0.0;
		double realization;
		Set<InnerNode> informationSet = parent.getInformationSet().getAllNodes();
		//System.out.println("12");
		for(InnerNode node : informationSet){
			//System.out.println("aa");
			parentSum += node.getSecurelyNbSamplesForPlayer(playerIndex);
			//System.out.println("bb"); // on nemusi byt INNERNODE (asi)
			if(node.getChildren()!=null && node.getChildOrNull(parentAction)!=null && node.getChildOrNull(parentAction) instanceof InnerNode && ((InnerNode)node.getChildOrNull(parentAction)).hasNbElement(playerIndex))
				sum+=((InnerNode)node.getChildOrNull(parentAction)).getNbSamplesForPlayer(playerIndex);
			else{
				//System.out.println("cc");
				// return uniform ditribution
				return 1.00/(double)parent.getActions().size();
			}
		}
		
		//System.out.println("13");
		
		realization = sum / parentSum;
		if(realization > 1.00){
			System.out.printf("[Armin F] Opponents realization unreasonable: %f / %f. \n",sum, parentSum);
			return 1.00/(double)parent.getActions().size();
		}
		if(realization == 0){
			System.out.println("[Armin F] Opponents realization unprovable.");
			return 1.00/(double)parent.getActions().size();
		}
		return realization;
	}
	
	

	private void giveActionToInformationSet(ArrayList<InformationSetClassArmin> inputISlist){
		for (InformationSetClassArmin is : inputISlist){
			is.setChosenAction(is.getStates().get(0).getChosenAction());
		}
	}
	
	
	
	/**
	 * This function is set a value of a state in IS to the IS. 
	 * This method must be used after checkInformationSetValue for the correctness
	 * It also uses a general variable called InformationSetValue 
	 * ANd must be used after populating the nodes
	 * 
	 * This method is not efficent and can be more efficent
	 * @param ISArmin
	 */
	private void populateInformationSetValues(HashMap<List<Integer>, List<InnerNode>> ISArmin){
		
		// The new Information Set object
		for (InformationSetClassArmin is : informationS){
			int tempValue = 0;
			for (InnerNode theNode : is.getStates()){
				tempValue += theNode.getParentRandomAct();
			}
			is.setRandomChoices(tempValue);
		}
		
		
		
		for (InformationSetClassArmin is : informationS){
			int tempValue = 0;
			for (InnerNode theNode : is.getStates()){
				tempValue += (theNode.getParentRandomAct() * theNode.getNodeValue());
			}
			int normalisationValue = (tempValue/is.getRandomChoices());
			is.setValue(normalisationValue);		
		}

		///------------------
		
		
		for (List<Integer> theKey : ISArmin.keySet()){
			
			int tempValue = 0;
			for (InnerNode theNode : ISArmin.get(theKey)){
				// Important to remember that parent Random act is important
				// self randomAct is just for the next node
				tempValue += theNode.getParentRandomAct();
			}
			InformationSetRandomChoices.put(theKey, tempValue);
		}
		
		
		for (List<Integer> theKey : ISArmin.keySet()){
			int tempValue = 0;
			for (InnerNode theNode : ISArmin.get(theKey)){
				// Important to remember that parent Random act is important
				// self randomAct is just for the next node
				tempValue += (theNode.getParentRandomAct() * theNode.getNodeValue());
			}
			int normalisationValue = (tempValue/(InformationSetRandomChoices.get(theKey)));
			
			InformationSetValue.put(theKey, normalisationValue);
		}
	}
	
	/**
	 * This method just check to see if the states in the information set all have the same values. They
	 * all must have the same values
	 */
	private  void checkInformationSetValue(HashMap<List<Integer>, List<InnerNode>> ISArmin){
		for (List<InnerNode> listIN : ISArmin.values()){
			Integer theValue = -1;
			for (InnerNode innerN : listIN){
				if (theValue == -1) theValue = innerN.getNodeValue();
				if (theValue != innerN.getNodeValue()){			
					System.out.println("ERROR values of the nodes are not the same\nTheValue is:" + theValue + " and the innerN.getNodeValue() is:" + innerN.getNodeValue());
					System.out.println("The node is:" + innerN.getGameState().getHistory());
					System.exit(0);
				}
			}
		}
	}
	
	
	private void printInformationSet(HashMap<List<Integer>, List<InnerNode>> inputIS){
		//\draw[dashed,thick,bend] (apple) --  node[above] {P2} (banana);
		//for(List<InnerNode> listIN : inputIS.values()){
		for(List<Integer> theKey : inputIS.keySet()){
			List<InnerNode> listIN = inputIS.get(theKey);
			System.out.println("These are nodes in an IS:");
			for(int i = 0; i < (listIN.size() - 1); i++){
				this.latexDrawInformationSets += "\\draw[dashed,thick,bend left=30] (" + listIN.get(i).getGameState().getHistory().hashCode()+") to node[above] {" + InformationSetValue.get(theKey) + "} (" + listIN.get(i+1).getGameState().getHistory().hashCode() +");\n";
				System.out.println("\t\t" + listIN.get(i).getGameState().getHistory().hashCode());
			}
		}
		
		///// new information set Object
		System.out.println("This is the new Information Set");
		
		for ( InformationSetClassArmin is : informationS){
			System.out.println("IS, value: " + is.getValue());
			for (InnerNode sta : is.getStates()){
				System.out.println(sta.getGameState().getHistory());
			}
		}
		
		///
	}
	
	
	/*
	\begin{bmatrix}
    X_{t_{k}} \\
    Y_{t_{k}} \\
    \dot{X}_{t_{k}}\\
    \dot{Y}_{t_{k}}
   \end{bmatrix}
	*/
	private void printTree(Node node){
		//[$\begin{bmatrix}(0.5) \\0 \\0\end{bmatrix}$  /Do
		latexDrawForest += "[";
		//latexDrawForest += node.getNodeValue() + "/";// + node.getParentRandomAct() + ":" + node.getUtilityNormalisation() + ":" + node.getNumberOfNodesInIS();
		latexDrawForest += "$\\begin{bmatrix}";
		for (int i=0 ; i < node.getMultiNodeValue().length ; i++){
			if (i != 0){ latexDrawForest +="\\\\";}
			if (i == node.getGameState().getPlayerToMove().getId()){
				latexDrawForest += "(" +  node.getMultiNodeValue()[i] + ")";
			}else{
				latexDrawForest +=  node.getMultiNodeValue()[i] + " ";
			}
		}
		latexDrawForest += "\\" + "end{bmatrix}$"; 
		if (  ( (int) node.getMultiNodeValue()[node.getGameState().getPlayerToMove().getId()] ) != node.getNodeValue()){
			latexDrawForest += "\\ ERROR!" + "\\ " + node.getNodeValue()+ " \\ ";
		}
		/*for ( int tempV : node.getFinalValueForTemps()){
			latexDrawForest += "|" + tempV;
		}*/
		if (!node.getGameState().isGameEnd() && node instanceof InnerNode){
						
			InnerNode inNode = (InnerNode) node ;
			latexDrawForest += "Do:" + inNode.getChosenAction();// + " History:" + node.getGameState().getHistory(); //+ "-" + "Inner";
			latexDrawForest += ", " +this.coloursDraw[node.getGameState().getPlayerToMove().getId()];
			if (node.getLastAction() != null){
				latexDrawForest += ",name=" + node.getGameState().getHistory().hashCode() + ",edge label={node[midway,sloped,font=\\tiny,above] {" + node.getLastAction().toString() +"}}";
			}
			//,edge label={node[midway,left] {Help!}}
			latexDrawForest += "\n";
			System.out.println("Inner:" + node.getNodeValue() + "  " + node.getGameState().getHistory());
			for( Node n : node.getArminChildren().values()){
				printTree(n);
			}
		}else{
			latexDrawForest += "-" + "Leaf";
		}
		latexDrawForest += "]";
		
		System.out.println("");

		
		return;
		
	}
	
	
	
	
	private List<InnerNode> statesToIS( InnerNode node){
		LinkedList<InnerNode> resultIS = new LinkedList<InnerNode>(); 
		List<Integer> nodeKey = new ArrayList<Integer>();		
		int inputNodePercetion = ((GGPGameState)node.getGameState()).getPerceptHistory().get(allRoles.get(node.getPlayerToMove().getId())).hashCode();
		int inputNodeLevel = node.getNodeLevel();
		nodeKey.add(inputNodeLevel);
		nodeKey.add(inputNodePercetion);
		nodeKey.add(node.getGameState().getHistory().getSequenceOf(node.getPlayerToMove()).hashCode());
		
		return InformationSetArmin.get(nodeKey);
	}
	
	private double[] searchAndPopulateLeafNodesValues(LeafNode node){
		
		//System.out.println("I have reached the last node and Last Node is " + node.getGameState().getHistory() + "size of Infomration Set: ");
		System.out.println("LeafNodeLevel1");
		int utility = (int) node.getGameState().getUtilities()[myself.getId()];
		double[] multiUtilValues =  node.getGameState().getUtilities();
		System.out.println("LeafNodeLevel1.1");
		// The part below was the bug which did not work for monty Hall problem
		int utilityAfterConsideringRandromAct = utility/(node.getParentRandomAct());
		//node.setNodeValue(utilityAfterConsideringRandromAct);
		node.setNodeValue(utility);
		node.setMultiNodeValue(multiUtilValues);
		System.out.println("LeafNodeLevel2");
		System.out.println(":::::::finalValue: " + node.getGameState().getUtilities()[myself.getId()] + "  MyselfIndex: " + myselfIndex);
		System.out.println("LeafNodeLevel3");
		System.out.println("LeafNodeLevel4");
		//return utilityAfterConsideringRandromAct;
		return multiUtilValues;
	}
	/*private int searchAndPopulateNodesValueCheck(Node node){
		if (node.getGameState().isGameEnd()){
			return searchAndPopulateLeafNodesValues(node);
		}else{
			return searchAndPopulateInnerNodesValues
		}
		
	}*/

	
	private double[] searchAndPopulateInnerNodesValues(InnerNode node){

		System.out.println("it came here at seasrchAndPopulateInnerNodesvalues and Node is: "  + node.getGameState().getHistory());
				
		
		//if (node.getGameState().getPlayerToMove().getId() !=  myself.getId()){
		// this part is related to random player
		if (node.getGameState().getPlayerToMove().getId() ==  GGPGameInfo.randomPlayerIndex ){
			// Now it's only random player who plays This part needs to be change
		//if (node.getGameState().isPlayerToMoveNature()){	
			int avgValue = 0;
			double[] avgUtils = null;
			Node nchild = null;
			for ( Action a1 : node.getArminChildren().keySet() ){
				nchild = node.getArminChildren().get(a1);
				
				if ((nchild instanceof InnerNode) &&  !nchild.getGameState().isGameEnd() ){
					System.out.println("level5.0r");
					InnerNode inChild = (InnerNode) nchild ;
					System.out.println("level5.1r");
					
					//tempValue +=  ( searchAndPopulateInnerNodesValues(inChild) / inChild.getParentRandomAct());
					//avgValue +=  searchAndPopulateInnerNodesValues(inChild);
					avgUtils = MathThing.addTwoArray(avgUtils, searchAndPopulateInnerNodesValues(inChild));

					
					System.out.println("level5.2r");

				}else{
					System.out.println("Good One  action:" + a1 + " from node: " + node.getGameState().getHistory());
					System.out.println("level6.0r");

					LeafNode leChild = (LeafNode) nchild;
					System.out.println("level6.1r");

					//tempValue += (searchAndPopulateLeafNodesValues(leChild) / leChild.getParentRandomAct());
					//avgValue += searchAndPopulateLeafNodesValues(leChild); 
					avgUtils = MathThing.addTwoArray(avgUtils, searchAndPopulateLeafNodesValues(leChild));
					
					System.out.println(">>r>tempValue:"+avgValue);
					System.out.println("level6.2r");

				}
				
							
				
			}
			//avgValue = avgValue /node.getArminChildren().keySet().size();
			avgUtils = MathThing.devideArraysElements(avgUtils, node.getArminChildren().keySet().size());
			
			System.out.println(">>r avgUtils averageValueByRandom:"+avgUtils[node.getPlayerToMove().getId()]);
			node.setNodeValue((int) avgUtils[node.getPlayerToMove().getId()]);
			node.setMultiNodeValue(avgUtils);
			return avgUtils;
			
			
		}else{
			// hHis is when the player is not nature
			// It can be us or opponent, here it assumed opponent has the same info as us.
			// AT the moment it's just us not anyone else
		
			int MaxValue = -1;
			Action theChosenAction = null;

			// Normalisation is part of finding the correctness of one state in the world
			// Is can be done by adding number of Random move to the state BN= 1/randomM1 + 1/randomM1 +...
			// and then to get normalisation do Normalisation = 1/BN; 
			// then multiply it to the results.
			float buttomNormalisation = 1; // buttomNormalisation must be fixed
			
			
			int normalisation = 1;
			for( Action action : node.getActions() ){
				System.out.println("level1");
				int tempValue= 0;
				double[] avgUtils = null;

				
				for ( InnerNode n : statesToIS(node) ){ // here it takes the states with same IS
					System.out.println("level2");
					System.out.println("tempValue -> "+ tempValue);
					Node nchild = null;
					
					// This part just tries to find children that are done by  "action" just 
					for ( Action a1 : n.getArminChildren().keySet() ){
						if ( a1.toString().equals(action.toString())){
							nchild  = n.getArminChildren().get(a1);
							break;
						}
					}
					//
					
					
					
					System.out.println("Number of Children:" + n.getArminChildren().size() );

					if(nchild == null){
						
						
						System.out.println("nchild IS NULL !");
						System.out.println("level2.1");
						System.out.println("Problematic -> action:" + action + " from node: " + n.getGameState().getHistory());
						for (Node n1 : n.getArminChildren().values() ){
							//System.out.println("the action is:" +a1 + " AND child is:" + n.getChildren().get(a1).getGameState().getHistory() );
							System.out.println("n1 is:" + n1.getGameState().getHistory());
						}
						for(Action a1 : n.getArminChildren().keySet()){
							System.out.println("level2.2");
							System.out.println("a1 is:" + a1 + "action is:" + action);
							System.out.println("a1 is action:" + a1.equals(action));
							System.out.println("IS for a1:" + a1.getInformationSet() + " " + a1.getInformationSet().hashCode() + " | IS for action:" + action.getInformationSet() + " " + action.getInformationSet().hashCode());
						}
						System.exit(0);
						
					}
					
					System.out.println("BNBNBN: buttomNormalisation before:" + buttomNormalisation);
					
					//This part is normalisation for now, it's removed
					//buttomNormalisation += (1.0/((float) nchild.getParentRandomAct()));
					
					
					System.out.println("The nChild:"+ nchild.getGameState().getHistory());
					System.out.println("BNBNBN: value added:" + (1/(nchild.getParentRandomAct())) + "  ValueOfParent:" + nchild.getParentRandomAct());
					System.out.println("BNBNBN: buttomNormalisation after:" + buttomNormalisation);
					
					System.out.println("level3");
					//System.out.println("CHECKING FOR CHILD: " + nchild.getGameState().getHistory());
					System.out.println("level4");

					if ((nchild instanceof InnerNode) &&  !nchild.getGameState().isGameEnd() ){
						System.out.println("level5.0");
						InnerNode inChild = (InnerNode) nchild ;
						System.out.println("level5.1");
						double[] tempUtils =  MathThing.devideArraysElements(searchAndPopulateInnerNodesValues(inChild), inChild.getParentRandomAct());
						avgUtils = MathThing.addTwoArray(avgUtils, tempUtils);
						//tempValue +=  ( searchAndPopulateInnerNodesValues(inChild) / inChild.getParentRandomAct());
						//tempValue +=  searchAndPopulateInnerNodesValues(inChild);
						
						System.out.println("level5.2");

					}else{
						System.out.println("Good One  action:" + action + " from node: " + n.getGameState().getHistory());
						System.out.println("level6.0");

						LeafNode leChild = (LeafNode) nchild;
						System.out.println("level6.1");

						//tempValue += (searchAndPopulateLeafNodesValues(leChild) / leChild.getParentRandomAct());
						//tempValue += searchAndPopulateLeafNodesValues(leChild); 
						double[] tempUtils =  MathThing.devideArraysElements(searchAndPopulateLeafNodesValues(leChild), leChild.getParentRandomAct());
						avgUtils = MathThing.addTwoArray(avgUtils, tempUtils);
						
						
						System.out.println(">>tempValue:"+avgUtils[node.getPlayerToMove().getId()]);
						System.out.println("level6.2");

					}
				}
				
				if (buttomNormalisation == 0){
					System.out.println("ERROR, buttomNormalisation must not be zero but it is now");
					System.exit(0);
				}else{
					System.out.println("BNBNBN: buttonNormalisation is not zero and it is " + buttomNormalisation);
				}
				normalisation = (int) (1/( buttomNormalisation));
				
				System.out.println("level7.1");

				//tempValue = tempValue /statesToIS(node).size();
				avgUtils = MathThing.devideArraysElements(avgUtils, statesToIS(node).size());
				System.out.println("level7.2");

				
				//tempValue = tempValue * normalisation;
				
				//node.addFinalValueForTemps(tempValue);
				node.addFinalValueForTemps((int) avgUtils[node.getPlayerToMove().getId()]);
				System.out.println("level7.3");
				
				System.out.println("tempValue is:" + avgUtils[node.getPlayerToMove().getId()] + " MaxValue is:" + MaxValue + " ChosenAction: " + action);

				if (avgUtils[node.getPlayerToMove().getId()] > MaxValue){
					
					MaxValue = (int) avgUtils[node.getPlayerToMove().getId()];
					theChosenAction = action;
				}
			}
			
			//n.getArminChildren().get(a1);
			System.out.println("level7.4");

			for (InnerNode n : statesToIS(node)){
				System.out.println("level7.5");

				n.setChosenAction(theChosenAction);
				System.out.println("level7.6");
				if ( theChosenAction == null){
					System.out.println("The Chosen Action is NULL");
					System.exit(1);
				}
				System.out.println("theCHosen Action is: " + theChosenAction.toString() );
				
				// This part just tries to find children that are done by an action just 
				Node theChosenChild = null;
				/*for ( Action a1 : n.getArminChildren().keySet() ){
					System.out.println("level7.6.1");
					if ( a1.toString().equals(theChosenAction.toString())){
						System.out.println("level7.6.2");
						theChosenChild  = n.getArminChildren().get(a1);
						break;
					}
					System.out.println("level7.6.3");
				}
				*/
				theChosenChild = n.getArminChildren().get(theChosenAction);
				if (theChosenChild == null){
					System.out.println("The Chosen child is NULL");
					System.exit(1);
				}
				//
				
				
				System.out.println("The node itself is: " + n.getGameState().getHistory());
				System.out.println("The Chosen Child :" +theChosenChild.getGameState().getHistory() );
				System.out.println("The value of Chosen child:" + theChosenChild.getNodeValue());
				System.out.println("TheChosenChild: " + theChosenChild.getGameState().getHistory());
				if (theChosenChild.getMultiNodeValue() == null){
					System.out.println("The chosen child multiNodeValue is Null");
				}
				
				for ( int i= 0; i < theChosenChild.getMultiNodeValue().length ; i++){
					System.out.println("The Utiliy of " + i + " : "  + theChosenChild.getMultiNodeValue()[i]);
				}
				System.out.println("The Utiliy of player to move of Chosen child:" + theChosenChild.getMultiNodeValue()[node.getPlayerToMove().getId()]);

				
				n.setNodeValue((int)theChosenChild.getMultiNodeValue()[n.getGameState().getPlayerToMove().getId()]);
				n.setMultiNodeValue(theChosenChild.getMultiNodeValue());
				System.out.println("level7.7");

			}
			System.out.println("level7.8");

			node.setUtilityNormalisation(normalisation);
			node.setNumberOfNodesInIS(statesToIS(node).size());
			//node.setNodeValue(MaxValue);
			
			//System.out.println(":::::::nodeValue: " + MaxValue);
			//return MaxValue;	
			System.out.println(":::::::nodeValue: " + node.getNodeValue());
			System.out.println("level7.9");

			return node.getMultiNodeValue();
			
			
			
		}
	}
	//}
	
	
private void SearchTheGameTreeLeaf(LeafNode node){

		/*
		int theHashCode = ((GGPGameState)node.getGameState()).getPerceptHistory().get(myselfRole).hashCode();
		//int[] theKey = {node.getNodeLevel(), theHashCode};
		
		List<Integer> theKey = new ArrayList<Integer>();
		theKey.add(node.getNodeLevel());
		theKey.add(theHashCode);
		if (InformationSetArmin.get(theKey) == null){
			List<InnerNode> tempLinkedList = new LinkedList<InnerNode>();
			tempLinkedList.add(node);
			InformationSetArmin.put(theKey, tempLinkedList );
			
		}else{
			InformationSetArmin.get(theKey).add(node);
		}
		
		for ( Player p :GGPGameInfo.ALL_PLAYERS){
			System.out.println(">> "+ p.toString() + " " + p.getId());
		}
		//if(!node.getGameState().getPlayerToMove().toString().equals("Pl0")){
		//}	
		System.out.println("node Level: " + node.getNodeLevel());
		System.out.println("player to Move: " + (node.getGameState().getPlayerToMove()).toString());
		System.out.print("Hash: " + ((GGPGameState)node.getGameState()).getPerceptHistory().get(myselfRole).hashCode() + " ");
		System.out.println("?!?!? Current InnerNode " + node.getGameState().getHistory() );
		System.out.println("((GGPGameState)stackNode.getGameState()).getCurrentTreeDepth(): " + ((GGPGameState)node.getGameState()).getCurrentTreeDepth());
		
		
	*/
		System.out.println("Utilities: " +node.getGameState().getUtilities()[myself.getId()] + "  "  +node.getGameState().getHistory() + " " + node.getGameState().toString() + " " + node.getGameState().isGameEnd());
		

			//System.out.println("?!?!?! LastNode : " + node.getGameState().getHistory());
		return;	
		
	}	
	
	private void SearchTheGameTree(InnerNode node){

		
		System.out.println("insideSearchTheGameTree Line1");
		
		boolean isExists = false;
		
		if (node.getGameState().isGameEnd()) {
			System.out.println("I have reached the last node");
			latexDraw += "]";
			System.out.println("Utilities: " +node.getGameState().getUtilities()[myself.getId()]);


			//System.out.println("?!?!?! LastNode : " + node.getGameState().getHistory());
			return;	
		}
		
		
		//Role currenRole = (GGPGameState)node.getGameState())
		// THIS IS IMPORTANT
		//
		
		System.out.println("insideSearchTheGameTree Line2");
		
		System.out.println("player to move: " + node.getPlayerToMove().toString());
		System.out.println("allRoles: " + allRoles);
		
		int theHashCode = ((GGPGameState)node.getGameState()).getPerceptHistory().get(allRoles.get(node.getPlayerToMove().getId())).hashCode();
		
		System.out.println("insideSearchTheGameTree Line3");

		//int[] theKey = {node.getNodeLevel(), theHashCode};
		List<Integer> theKey = new ArrayList<Integer>();
		
		// The new informationSet class object
		for (InformationSetClassArmin isca : informationS){
			if (isca.doesKeyMatch(node.getNodeLevel(), theHashCode,node.getGameState().getHistory().getSequenceOf(node.getPlayerToMove()).hashCode())){
				isca.addState(node);
				isExists = true;
				break;
			}
		}
		if (!isExists){
			// The final attribute is for the new player coordination so I just add 0
			InformationSetClassArmin tempIS = new InformationSetClassArmin(node.getNodeLevel(), theHashCode, node.getGameState().getHistory().getSequenceOf(node.getPlayerToMove()).hashCode(),0,node.getPastActions());
			tempIS.addState(node);
			informationS.add(tempIS);
		}
		// ---------------------------------
		
		theKey.add(node.getNodeLevel());
		theKey.add(theHashCode);
		theKey.add(node.getGameState().getHistory().getSequenceOf(node.getPlayerToMove()).hashCode());
		if (InformationSetArmin.get(theKey) == null){
			List<InnerNode> tempLinkedList = new LinkedList<InnerNode>();
			tempLinkedList.add(node);
			InformationSetArmin.put(theKey, tempLinkedList );
			
		}else{
			InformationSetArmin.get(theKey).add(node);
		}
		
		for ( Player p :GGPGameInfo.ALL_PLAYERS){
			System.out.println(">> "+ p.toString() + " " + p.getId());
			
		}
		//if(!node.getGameState().getPlayerToMove().toString().equals("Pl0")){
		//}	
		System.out.println("node Level: " + node.getNodeLevel());
		System.out.println("player to Move: " + (node.getGameState().getPlayerToMove()).toString());
		System.out.print("Hash: " + ((GGPGameState)node.getGameState()).getPerceptHistory().get(allRoles.get(node.getPlayerToMove().getId())).hashCode() + " ");
		System.out.println("?!?!? Current InnerNode " + node.getGameState().getHistory() );
		System.out.println("((GGPGameState)stackNode.getGameState()).getCurrentTreeDepth(): " + ((GGPGameState)node.getGameState()).getCurrentTreeDepth());
		
		
		
		// It was here 
		
		
		// ^^^^^^^^^
		
		
		// THis isn't always good but should be changed
		for( Action action : node.getActions() ){
			latexDraw += "["+ action.toString();// + ", edge="+ coloursDraw[node.getGameState().getPlayerToMove().getId()] + " " ;
			GameState nextState = node.getGameState().performAction(action);

			System.out.println("SearchTreeAndAdding: " + "now child of:" + node.getGameState().getHistory() + " by action:" + action+ " and child is:" + nextState.getHistory());
			
			if (nextState.isGameEnd()) {
				LeafNode LeafchildNode =  new LeafNode(node, nextState, action);
				node.putChild(action, LeafchildNode);
				SearchTheGameTreeLeaf(LeafchildNode);
				
				//InnerNode LeafchildNode =  new InnerNode(node, nextState, action);
				//SearchTheGameTree(LeafchildNode);
				return;

			}

			InnerNode childNode;
			//if (nextState.isPlayerToMoveNature()) {
			//	childNode=  new ChanceNode(node, nextState, action);
			//}
			//else{
			childNode = new InnerNode(node, nextState, action);
			//}

			node.putChild(action, childNode);
			SearchTheGameTree(childNode);

		}
		
		latexDraw += "]\n";
		return;
	}
	
	
	
	/**
	 * edited By Armin Chitizadeh
	 * 
	 * @param currentISInput It's the current Information set > The function recreate a new one called "nextIS" it's inside the wrapper return object
	 * @param gameTreeDepthInput It increases outside of the function by one at each round of the game.
	 * @param currentMatchInput It only uses it to get the perception hash in the first line
	 * @param playedActions This is our action and is updated outside the function and has this form > (GGPAction)currentIS.get(0).getActions().get(0);
	 * @param roleOfMyselfInput These are pretty much the same for us as player
	 * @param myselfPlayerInput Should be redundant I think
	 * @param InputBelief This is used to decide on which move to take. It is updated inside the function and attached to the return function
	 * @param counter I have added to keep the function calling the last node
	 * @return 
	 */
	private BeliefISWrapper updateCurrentStatesInnerLoop( ArrayList<InnerNode> currentISInput, int gameTreeDepthInput, Match currentMatchInput, Sequence playedActions, Role roleOfMyselfInput, Player myselfPlayerInput, ArrayList<Double> InputBelief, int counter ){
		int perceptHash = currentMatchInput.getPerceptHistory().hashCode();
		int index = 0;
		double nextBeliefsSum = (double)0;
		double currentBeliefsSum = (double)0;
		double randomTurn = (double)1.00;
		double rootBelief;
		boolean leadsToNextIS;
		boolean isOpponentNode;
		Stack<InnerNode> stack = new Stack<InnerNode>();
		ArrayList<InnerNode> nextIS = new ArrayList<InnerNode>();
		ArrayList<Double> nextBelief = new ArrayList<Double>();
		InnerNode stackNode;
		Node nextNode;
		InnerNode parentNode = null;
		Action parentAction = null;
		
		for(InnerNode ISnode : currentISInput){
			System.out.println("&&&&& currentIS.size: " + currentISInput.size() );
			
			leadsToNextIS = false;
			rootBelief = InputBelief.get(index);
			//stack.add(ISnode);
			randomTurn = 1.00;
			parentAction = null;
			parentNode = null;
			isOpponentNode = false;
			System.out.println("gameTreeDepth: " + gameTreeDepthInput);
			
			if(gameTreeDepthInput>0){ // <><>Only time it's 0 is at the first move
				if(ISnode.getChildren()==null){
					System.out.println("It came to set nextNode to null");
					nextNode = null;
				}else	
					nextNode = ISnode.getChildOrNull(playedActions.getLast());
				if(nextNode == null)
					nextNode = ISnode.getNewChildAfter(playedActions.getLast());
				//System.out.println("6");
				if(nextNode instanceof InnerNode)
					stack.push((InnerNode)nextNode);
				
				System.out.println("<>nextNode: " + nextNode.getGameState().getHistory());
				System.out.println("<>ISnode: " +ISnode.getGameState().getHistory());
			}
			else
				stack.add(ISnode);
			

		
			//System.out.println("1");
			
			if(rootBelief == 0)
				System.out.println("Zero root belief");
			
			

			while(!stack.isEmpty()){ //<><> This always goes once, stack size is always 1 
				System.out.println("///// stack size is : " + stack.size());

				stackNode = stack.pop();
				//<><> The only time that gameTreeDepth are equal is when at the start of the game 
				System.out.println("///// stacknodeCurrentTreeDepth is : " + ((GGPGameState)stackNode.getGameState()).getCurrentTreeDepth());
				System.out.println("////  gameTreeDepth: " + gameTreeDepthInput);
				if(gameTreeDepthInput==((GGPGameState)stackNode.getGameState()).getCurrentTreeDepth()){ 
					if (counter == 0){
						System.out.println("@@@@@ perceptHash: " + perceptHash);
						
					}
					if(perceptHash != ((GGPGameState)stackNode.getGameState()).getPerceptHistory().get(roleOfMyselfInput).hashCode())
						continue;
					else{		
						//System.out.println("2");
						if(stackNode.getGameState().getPlayerToMove().equals(myselfPlayerInput)){ //<>If it's my turn to move
							if(randomTurn==0)
								System.out.println("[ShodanArmin] Random turn with zero probability!");
							nextBelief.add(rootBelief*randomTurn*getOpponentRealization(parentNode, parentAction));
							
							System.out.println("######stackNode which is added to nextIS: " + stackNode.getGameState().getHistory());
							
							nextIS.add(stackNode);
							leadsToNextIS = true;
							//beliefSum += rootBelief;	
							//System.out.println("4");
						}
						else{
							//System.out.println("3");
							if(stackNode.getGameState().isPlayerToMoveNature()){
								randomTurn = 1.00/(stackNode.getActions().size());							
							}
							else{
								parentNode = stackNode;
								isOpponentNode = true;
							}
							for (Action action : stackNode.getActions()){	
								//System.out.println("5");
								if(isOpponentNode)
									parentAction = action;				
								if(stackNode.getChildren()==null)
									nextNode = null;
								else	
									nextNode = stackNode.getChildOrNull(action);
								if(nextNode == null)
									nextNode = stackNode.getNewChildAfter(action);
								//System.out.println("6");
								if(nextNode instanceof InnerNode)
									stack.push((InnerNode)nextNode);		
							}
						}			
					}
				}
				else{
					//System.out.println("7");
					if(stackNode.getGameState().isPlayerToMoveNature()){
						randomTurn = 1.00/(stackNode.getActions().size());				
					}
					else{
						parentNode = stackNode;
						isOpponentNode = true;
					}					
					for (Action action : stackNode.getActions()){	
						//System.out.println("8");
						if(isOpponentNode)
							parentAction = action;
						if(stackNode.getChildren()==null)
							nextNode = stackNode.getNewChildAfter(action);
						else	
							nextNode = stackNode.getChildOrNull(action);
						if(nextNode == null)
							nextNode = stackNode.getNewChildAfter(action);
						if(nextNode instanceof InnerNode)
							//findAllNodesInIS(perceptHash, nextIS,nextBelief,(InnerNode)nextNode, parentNode, parentAction, rootBelief, randomTurn);
							stack.push((InnerNode)nextNode);
					}
				}
				
			}
						
			index++;	
			if(leadsToNextIS)
				currentBeliefsSum += rootBelief;
			
			
		}
		
		
		
		//System.out.println("9");
		
		index = 0;
		for(InnerNode node : nextIS){
			nextBelief.set(index, nextBelief.get(index)/(currentBeliefsSum)); // no longer nSum here
			if(nextBelief.get(index)==0)
				System.out.printf("[ShodanArmin] Err: State with zero belief.\n");
			nextBeliefsSum += nextBelief.get(index);
			node.setParent(null);
			index++;
		}
		for(int i = 0; i < nextIS.size(); i++)
			nextBelief.set(i, nextBelief.get(i)/nextBeliefsSum);
		
		//belief = nextBelief;
		
		BeliefISWrapper updatedBIS = new BeliefISWrapper(nextIS, nextBelief);
		return updatedBIS;
	}
	
	
	
	private BeliefISWrapper updateCurrentStatesFunction( ArrayList<InnerNode> currentISInput, int gameTreeDepthInput, Match currentMatchInput, Sequence playedActions, Role roleOfMyselfInput, Player myselfPlayerInput, ArrayList<Double> InputBelief ){
		int perceptHash = currentMatchInput.getPerceptHistory().hashCode();
		int index = 0;
		double nextBeliefsSum = (double)0;
		double currentBeliefsSum = (double)0;
		double randomTurn = (double)1.00;
		double rootBelief;
		boolean leadsToNextIS;
		boolean isOpponentNode;
		Stack<InnerNode> stack = new Stack<InnerNode>();
		ArrayList<InnerNode> nextIS = new ArrayList<InnerNode>();
		ArrayList<Double> nextBelief = new ArrayList<Double>();
		InnerNode stackNode;
		Node nextNode;
		InnerNode parentNode = null;
		Action parentAction = null;
		
		for(InnerNode ISnode : currentISInput){
			System.out.println("&&&&& currentIS.size: " + currentISInput.size() );
			
			leadsToNextIS = false;
			rootBelief = InputBelief.get(index);
			//stack.add(ISnode);
			randomTurn = 1.00;
			parentAction = null;
			parentNode = null;
			isOpponentNode = false;
			System.out.println("gameTreeDepth: " + gameTreeDepthInput);
			
			if(gameTreeDepthInput>0){ // <><>Only time it's 0 is at the first move
				if(ISnode.getChildren()==null){
					System.out.println("It came to set nextNode to null");
					nextNode = null;
				}else	
					nextNode = ISnode.getChildOrNull(playedActions.getLast());
				if(nextNode == null)
					nextNode = ISnode.getNewChildAfter(playedActions.getLast());
				//System.out.println("6");
				if(nextNode instanceof InnerNode)
					stack.push((InnerNode)nextNode);
				
				System.out.println("<>nextNode: " + nextNode.getGameState().getHistory());
				System.out.println("<>ISnode: " +ISnode.getGameState().getHistory());
			}
			else
				stack.add(ISnode);
			

		
			//System.out.println("1");
			
			if(rootBelief == 0)
				System.out.println("Zero root belief");
			
			

			while(!stack.isEmpty()){ //<><> This always goes once, stack size is always 1 
				System.out.println("///// stack size is : " + stack.size());

				stackNode = stack.pop();
				//<><> The only time that gameTreeDepth are equal is when at the start of the game 
				System.out.println("///// stacknodeCurrentTreeDepth is : " + ((GGPGameState)stackNode.getGameState()).getCurrentTreeDepth());
				System.out.println("////  gameTreeDepth: " + gameTreeDepthInput);
				if(gameTreeDepthInput==((GGPGameState)stackNode.getGameState()).getCurrentTreeDepth()){ 
					if(perceptHash != ((GGPGameState)stackNode.getGameState()).getPerceptHistory().get(roleOfMyselfInput).hashCode())
						continue;
					else{		
						//System.out.println("2");
						if(stackNode.getGameState().getPlayerToMove().equals(myselfPlayerInput)){ //<>If it's my turn to move
							if(randomTurn==0)
								System.out.println("[ShodanArmin] Random turn with zero probability!");
							nextBelief.add(rootBelief*randomTurn*getOpponentRealization(parentNode, parentAction));
							
							System.out.println("######stackNode which is added to nextIS: " + stackNode.getGameState().getHistory());
							
							nextIS.add(stackNode);
							leadsToNextIS = true;
							//beliefSum += rootBelief;	
							//System.out.println("4");
						}
						else{
							//System.out.println("3");
							if(stackNode.getGameState().isPlayerToMoveNature()){
								randomTurn = 1.00/(stackNode.getActions().size());							
							}
							else{
								parentNode = stackNode;
								isOpponentNode = true;
							}
							for (Action action : stackNode.getActions()){	
								//System.out.println("5");
								if(isOpponentNode)
									parentAction = action;				
								if(stackNode.getChildren()==null)
									nextNode = null;
								else	
									nextNode = stackNode.getChildOrNull(action);
								if(nextNode == null)
									nextNode = stackNode.getNewChildAfter(action);
								//System.out.println("6");
								if(nextNode instanceof InnerNode)
									stack.push((InnerNode)nextNode);		
							}
						}			
					}
				}
				else{
					//System.out.println("7");
					if(stackNode.getGameState().isPlayerToMoveNature()){
						randomTurn = 1.00/(stackNode.getActions().size());				
					}
					else{
						parentNode = stackNode;
						isOpponentNode = true;
					}					
					for (Action action : stackNode.getActions()){	
						//System.out.println("8");
						if(isOpponentNode)
							parentAction = action;
						if(stackNode.getChildren()==null)
							nextNode = stackNode.getNewChildAfter(action);
						else	
							nextNode = stackNode.getChildOrNull(action);
						if(nextNode == null)
							nextNode = stackNode.getNewChildAfter(action);
						if(nextNode instanceof InnerNode)
							//findAllNodesInIS(perceptHash, nextIS,nextBelief,(InnerNode)nextNode, parentNode, parentAction, rootBelief, randomTurn);
							stack.push((InnerNode)nextNode);
					}
				}
				
			}
						
			index++;	
			if(leadsToNextIS)
				currentBeliefsSum += rootBelief;
			
			
		}
		
		
		
		//System.out.println("9");
		
		index = 0;
		for(InnerNode node : nextIS){
			nextBelief.set(index, nextBelief.get(index)/(currentBeliefsSum)); // no longer nSum here
			if(nextBelief.get(index)==0)
				System.out.printf("[ShodanArmin] Err: State with zero belief.\n");
			nextBeliefsSum += nextBelief.get(index);
			node.setParent(null);
			index++;
		}
		for(int i = 0; i < nextIS.size(); i++)
			nextBelief.set(i, nextBelief.get(i)/nextBeliefsSum);
		
		//belief = nextBelief;
		
		BeliefISWrapper updatedBIS = new BeliefISWrapper(nextIS, nextBelief);
		return updatedBIS;
	}
	
	
	private ArrayList<InnerNode> updateCurrentStates(){
		int perceptHash = getMatch().getPerceptHistory().hashCode();
		int index = 0;
		double nextBeliefsSum = (double)0;
		double currentBeliefsSum = (double)0;
		double randomTurn = (double)1.00;
		double rootBelief;
		boolean leadsToNextIS;
		boolean isOpponentNode;
		Stack<InnerNode> stack = new Stack<InnerNode>();
		ArrayList<InnerNode> nextIS = new ArrayList<InnerNode>();
		ArrayList<Double> nextBelief = new ArrayList<Double>();
		InnerNode stackNode;
		Node nextNode;
		InnerNode parentNode = null;
		Action parentAction = null;
		
		for(InnerNode ISnode : currentIS){
			leadsToNextIS = false;
			rootBelief = belief.get(index);
			//stack.add(ISnode);
			randomTurn = 1.00;
			parentAction = null;
			parentNode = null;
			isOpponentNode = false;
			
			if(gameTreeDepth>0){
				if(ISnode.getChildren()==null)
					nextNode = null;
				else	
					nextNode = ISnode.getChildOrNull(actionsPlayed.getLast());
				if(nextNode == null)
					nextNode = ISnode.getNewChildAfter(actionsPlayed.getLast());
				//System.out.println("6");
				if(nextNode instanceof InnerNode)
					stack.push((InnerNode)nextNode);
			}
			else
				stack.add(ISnode);
				
		
			//System.out.println("1");
			
			if(rootBelief == 0)
				System.out.println("Zero root belief");
			
			System.out.println("Above while");
			while(!stack.isEmpty()){
				
				//<><>
				System.out.println("<>Size of stack: " + stack.size());
				
				stackNode = stack.pop();
				if(gameTreeDepth==((GGPGameState)stackNode.getGameState()).getCurrentTreeDepth()){
					if(perceptHash != ((GGPGameState)stackNode.getGameState()).getPerceptHistory().get(allRoles.get(stackNode.getPlayerToMove().getId())).hashCode())
						continue;
					else{		
						//System.out.println("2");
						if(stackNode.getGameState().getPlayerToMove().equals(myself)){ //<>If it's my turn to move
							if(randomTurn==0)
								System.out.println("[Armin F] Random turn with zero probability!");
							nextBelief.add(rootBelief*randomTurn*getOpponentRealization(parentNode, parentAction));
							nextIS.add(stackNode);
							leadsToNextIS = true;
							//beliefSum += rootBelief;	
							//System.out.println("4");
						}
						else{
							//System.out.println("3");
							if(stackNode.getGameState().isPlayerToMoveNature()){
								randomTurn = 1.00/(stackNode.getActions().size());							
							}
							else{
								parentNode = stackNode;
								isOpponentNode = true;
							}
							for (Action action : stackNode.getActions()){	
								//System.out.println("5");
								if(isOpponentNode)
									parentAction = action;				
								if(stackNode.getChildren()==null)
									nextNode = null;
								else	
									nextNode = stackNode.getChildOrNull(action);
								if(nextNode == null)
									nextNode = stackNode.getNewChildAfter(action);
								//System.out.println("6");
								if(nextNode instanceof InnerNode)
									stack.push((InnerNode)nextNode);		
							}
						}			
					}
				}
				else{
					//System.out.println("7");
					if(stackNode.getGameState().isPlayerToMoveNature()){
						randomTurn = 1.00/(stackNode.getActions().size());				
					}
					else{
						parentNode = stackNode;
						isOpponentNode = true;
					}					
					for (Action action : stackNode.getActions()){	
						//System.out.println("8");
						if(isOpponentNode)
							parentAction = action;
						if(stackNode.getChildren()==null)
							nextNode = stackNode.getNewChildAfter(action);
						else	
							nextNode = stackNode.getChildOrNull(action);
						if(nextNode == null)
							nextNode = stackNode.getNewChildAfter(action);
						if(nextNode instanceof InnerNode)
							//findAllNodesInIS(perceptHash, nextIS,nextBelief,(InnerNode)nextNode, parentNode, parentAction, rootBelief, randomTurn);
							stack.push((InnerNode)nextNode);
					}
				}
				
			}
			
			
			index++;	
			if(leadsToNextIS)
				currentBeliefsSum += rootBelief;
			
			
			
		}
		
		
		
		//System.out.println("9");
		
		index = 0;
		for(InnerNode node : nextIS){
			nextBelief.set(index, nextBelief.get(index)/(currentBeliefsSum)); // no longer nSum here
			if(nextBelief.get(index)==0)
				System.out.printf("[Armin F] Err: State with zero belief.\n");
			nextBeliefsSum += nextBelief.get(index);
			node.setParent(null);
			index++;
		}
		for(int i = 0; i < nextIS.size(); i++)
			nextBelief.set(i, nextBelief.get(i)/nextBeliefsSum);
		
		belief = nextBelief;
		return nextIS;
	}

	@Override
	public void stop() throws StoppingException {
		gameInfo = null;
	    isPlayable = true;
	    rootState = null;
	    expander = null;
	    gameTreeDepth = 0;
	    myself = null;
	    myselfRole = null;
	    myselfIndex = -1;
	    currentIS = null;
	    belief = null;
	    actionsPlayed = null;	    
	    ggpMCTSConfig = null;
	    runner = null;	    
	    strategy = null;	    
	    generator = null;
	    beliefSum = 0.0;
	    nSum = 0;
	    System.gc();
		System.out.println("[Armin F] Game ended.");
	}

	@Override
	public void abort() throws AbortingException {
		// TODO Auto-generated method stub
		System.out.println("[Armin F] Game aborted.");
	}

	@Override
	public void preview(Game g, long timeout) throws GamePreviewException {
		// no need
		
	}

	@Override
	public String getName() {
		return "Shodan Armin Nash";
	}

}
