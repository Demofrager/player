package cz.agents.gtlibrary.ggp.gdlII.player;
// THIS IS JUST SOME CHANGES  <><> ARMIN CHITIZADEH <><>
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Set;
import java.util.Stack;

import org.ggp.base.player.gamer.Gamer;
import org.ggp.base.player.gamer.exception.AbortingException;
import org.ggp.base.player.gamer.exception.GamePreviewException;
import org.ggp.base.player.gamer.exception.MetaGamingException;
import org.ggp.base.player.gamer.exception.MoveSelectionException;
import org.ggp.base.player.gamer.exception.StoppingException;
import org.ggp.base.util.game.Game;
import org.ggp.base.util.gdl.grammar.GdlTerm;
import org.ggp.base.util.statemachine.Role;

import cz.agents.gtlibrary.algorithms.mcts.MCTSConfig;
import cz.agents.gtlibrary.algorithms.mcts.MCTSInformationSet;
import cz.agents.gtlibrary.algorithms.mcts.MCTSRunner;
import cz.agents.gtlibrary.algorithms.mcts.Simulator;
import cz.agents.gtlibrary.algorithms.mcts.distribution.FrequenceDistribution;
import cz.agents.gtlibrary.algorithms.mcts.nodes.InnerNode;
import cz.agents.gtlibrary.algorithms.mcts.nodes.Node;
import cz.agents.gtlibrary.algorithms.mcts.selectstrat.Exp3BackPropFactory;
import cz.agents.gtlibrary.ggp.gdlII.domain.prover.GGPAction;
import cz.agents.gtlibrary.ggp.gdlII.domain.prover.GGPExpander;
import cz.agents.gtlibrary.ggp.gdlII.domain.prover.GGPGameInfo;
import cz.agents.gtlibrary.ggp.gdlII.domain.prover.GGPGameState;
import cz.agents.gtlibrary.iinodes.ArrayListSequenceImpl;
import cz.agents.gtlibrary.interfaces.Action;
import cz.agents.gtlibrary.interfaces.Player;
import cz.agents.gtlibrary.interfaces.Sequence;
import cz.agents.gtlibrary.strategy.Strategy;
import cz.agents.gtlibrary.strategy.UniformStrategyForMissingSequences;

public class ShoDanGo extends Gamer{
	
    private static final int MCTS_ITERATIONS_PER_CALL = (int)10;
    private static final double gamma = 0.05;
    private static final boolean EXPORT = true;
	
    private GGPGameInfo gameInfo;
    private boolean isPlayable;
    private GGPGameState rootState;
    private GGPExpander<MCTSInformationSet> expander;
    
    private int gameTreeDepth;
    private Player myself;
    private Role myselfRole;
    private int myselfIndex;
    
    private ArrayList<InnerNode> currentIS;
    private ArrayList<Double> belief;
    private Sequence actionsPlayed;
    
    private MCTSConfig ggpMCTSConfig;
    private MCTSRunner runner;
    
    private Strategy strategy;
    
    private Random generator;
    
    //for computing next belief
    double beliefSum;
    int nSum;
    
	public static PrintWriter writer;
    
    
	@Override
	public void metaGame(long timeout) throws MetaGamingException {
		if(EXPORT)
			try {
				writer = new PrintWriter(getMatch().hashCode()+".dot", "UTF-8");
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (UnsupportedEncodingException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		if(EXPORT) writer.println("digraph tree {");
		gameInfo = null;
		try {
			gameInfo = new GGPGameInfo(getMatch().getGame().getRules());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		System.out.println("[Shodan V] Setting roles.");
		
		myself = GGPGameInfo.getPlayerByRole(getRoleName());
        myselfIndex = GGPGameInfo.getRoleIndexByRole(getRoleName());
        myselfRole = GGPGameInfo.roles.get(myselfIndex);
		gameTreeDepth = 0;
		
		if(myself == null || myselfRole==null)
			System.out.println("[Shodan V] Err: Couldn't instantiate roles.");
		
        if(GGPGameInfo.ALL_PLAYERS.length <= 3)
        	if(GGPGameInfo.randomPlayerIndex == -1 && GGPGameInfo.ALL_PLAYERS.length == 3)
        		isPlayable = false;
        	else isPlayable = true;
		else
			isPlayable = false;
        
        System.out.println("[Shodan V] Setting MCTS.");
        
        rootState = new GGPGameState();
        ggpMCTSConfig = new MCTSConfig(new Simulator(), new Exp3BackPropFactory(0,gameInfo.getMaxUtility(),gamma), new UniformStrategyForMissingSequences.Factory(), null);
        expander = new GGPExpander<MCTSInformationSet> (ggpMCTSConfig);
		runner = new MCTSRunner(ggpMCTSConfig, rootState, expander);
		
		System.out.println("[Shodan V] Initializing MCTS tree.");
		
		runner.runMCTS(0, myself); // this creates a rootstate 
		
		currentIS = new ArrayList<InnerNode>();
		belief = new ArrayList<Double>();
		actionsPlayed = new ArrayListSequenceImpl(myself);
		generator = new Random();
		
		belief.add((double)1.00);
		currentIS.add(runner.getRootNode());
        
        System.out.println("[Shodan V] Ready.");
	}

	@Override
	public GdlTerm selectMove(long timeout) throws MoveSelectionException {
		GGPAction action;
		GdlTerm move;
		int iteration = 0;
		long startTime;
		long oneRoundTime = 1;
		
		System.out.println("[Shodan V] Next Round!");
		System.out.printf("\t   Current percept history: %s \n",getMatch().getPerceptHistory().toString());
		currentIS = updateCurrentStates();
		System.out.printf("\t   IS size: %d \n",currentIS.size());
		
		if(!isPlayable) 
			action = (GGPAction)currentIS.get(0).getActions().get(0);
		else{
			System.out.println("[Shodan V] Running MCts.");
			startTime = timeout - System.currentTimeMillis();
			while((timeout - System.currentTimeMillis()) > (2*oneRoundTime+1000)){
				//System.out.println(iteration*MCTS_ITERATIONS_PER_CALL);
				runner.setRootNode(selectNode());
				runner.runMCTS(MCTS_ITERATIONS_PER_CALL, myself);
				//if(iteration==0)
					oneRoundTime = (startTime - (timeout - System.currentTimeMillis()))/(iteration+1);
				iteration++;
			}
			System.out.printf("[Shodan V] Iters: %d. \n",iteration*MCTS_ITERATIONS_PER_CALL);
			// create the strategy
			System.out.println("[Shodan V] Computing strategy.");
			strategy = runner.runMCTS(0, myself, new FrequenceDistribution());
		
			// select action
			System.out.println("[Shodan V] Selecting action.");
			action = selectAction();
		}
		move = action.getTerm();//GGPGameInfo.propNet.getInputProposition(action.getInputPropositionIndex()).getName().get(1);
		System.out.printf("\t   Playing: %s.\n",move.toString());
		gameTreeDepth++;
		actionsPlayed.addLast(action);
		return move;
	}
	
	// returns Node with distribution of believes in current IS
	private InnerNode selectNode(){
		double counter = (double) 0;
		double prob = generator.nextDouble();
		for(int i = 0; i< belief.size(); i++){
				counter+=belief.get(i);
				if(counter > prob)
					return currentIS.get(i);
			}
		System.out.println("[Shodan V] Err: No node selected.");
		return null;
	}
	
	private GGPAction selectAction(){
		double counter = (double) 0;
		double prob = generator.nextDouble();
		for(Sequence seq : strategy.keySet())
			if(seq.size()==1){
				counter+=strategy.get(seq);
				if(counter > prob){
					return (GGPAction)seq.getFirst();
				}
			}	
		System.out.println("[Shodan V] Err: No action selected.");
		return null;
	}
	
	// UZ OK
	private double getOpponentRealization(InnerNode parent, Action parentAction){
		
		if(parent==null)
			return 1.00;
		
		int playerIndex = ((GGPGameState)parent.getGameState()).getCurrentPlayerIndex();		
		double sum = 0.0;
		double parentSum = 0.0;
		double realization;
		Set<InnerNode> informationSet = parent.getInformationSet().getAllNodes();
		//System.out.println("12");
		for(InnerNode node : informationSet){
			//System.out.println("aa");
			parentSum += node.getSecurelyNbSamplesForPlayer(playerIndex);
			//System.out.println("bb"); // on nemusi byt INNERNODE (asi)
			if(node.getChildren()!=null && node.getChildOrNull(parentAction)!=null && node.getChildOrNull(parentAction) instanceof InnerNode && ((InnerNode)node.getChildOrNull(parentAction)).hasNbElement(playerIndex))
				sum+=((InnerNode)node.getChildOrNull(parentAction)).getNbSamplesForPlayer(playerIndex);
			else{
				//System.out.println("cc");
				// return uniform ditribution
				System.out.println("[Shodan V] Not enough iterations. Returning unifor prob.");
				return 1.00/(double)parent.getActions().size();
			}
		}
		
		//System.out.println("13");
		
		realization = sum / parentSum;
		if(realization > 1.00){
			System.out.printf("[Shodan V] Opponents realization unreasonable: %f / %f. \n",sum, parentSum);
			return 1.00/(double)parent.getActions().size();
		}
		if(realization == 0){
			System.out.println("[Shodan V] Opponents realization unprovable.");
			return 1.00/(double)parent.getActions().size();
		}
		return realization;
	}
	
	private ArrayList<InnerNode> updateCurrentStates(){
		int perceptHash = getMatch().getPerceptHistory().hashCode();
		int index = 0;
		double nextBeliefsSum = (double)0;
		double currentBeliefsSum = (double)0;
		double randomTurn = (double)1.00;
		double rootBelief;
		boolean leadsToNextIS;
		boolean isOpponentNode;
		Stack<InnerNode> stack = new Stack<InnerNode>();
		ArrayList<InnerNode> nextIS = new ArrayList<InnerNode>();
		ArrayList<Double> nextBelief = new ArrayList<Double>();
		InnerNode stackNode;
		Node nextNode;
		InnerNode parentNode = null;
		Action parentAction = null;
		
		for(InnerNode ISnode : currentIS){
			leadsToNextIS = false;
			rootBelief = belief.get(index);
			//stack.add(ISnode);
			randomTurn = 1.00;
			parentAction = null;
			parentNode = null;
			isOpponentNode = false;
			
			if(gameTreeDepth>0){
				if(ISnode.getChildren()==null)
					nextNode = null;
				else	
					nextNode = ISnode.getChildOrNull(actionsPlayed.getLast());
				if(nextNode == null)
					nextNode = ISnode.getNewChildAfter(actionsPlayed.getLast());
				//System.out.println("6");
				if(nextNode instanceof InnerNode){
					stack.push((InnerNode)nextNode);
					if (EXPORT)
						writer.println("\"" + ISnode.hashCode() + "\" -> \"" + nextNode.hashCode()+ "\" [label=\"" + ((GGPAction)actionsPlayed.getLast()).getTerm().toString() + "\"];");
				}
			}
			else
				stack.add(ISnode);
				
		
			//System.out.println("1");
			
			if(rootBelief == 0)
				System.out.println("Zero root belief");
			
			while(!stack.isEmpty()){
				stackNode = stack.pop();
				if(gameTreeDepth==((GGPGameState)stackNode.getGameState()).getCurrentTreeDepth()){
					if(perceptHash != ((GGPGameState)stackNode.getGameState()).getPerceptHistory().get(myselfRole).hashCode())
						continue;
					else{		
						//System.out.println("2");
						if(stackNode.getGameState().getPlayerToMove().equals(myself)){
							if(randomTurn==0)
								System.out.println("[Shodan V] Random turn with zero probability!");
							nextBelief.add(rootBelief*randomTurn*getOpponentRealization(parentNode, parentAction));
							nextIS.add(stackNode);
							leadsToNextIS = true;
							//beliefSum += rootBelief;	
							//System.out.println("4");
						}
						else{
							//System.out.println("3");
							if(stackNode.getGameState().isPlayerToMoveNature()){
								randomTurn = 1.00/(stackNode.getActions().size());							
							}
							else{
								parentNode = stackNode;
								isOpponentNode = true;
							}
							for (Action action : stackNode.getActions()){	
								//System.out.println("5");
								if(isOpponentNode)
									parentAction = action;				
								if(stackNode.getChildren()==null)
									nextNode = null;
								else	
									nextNode = stackNode.getChildOrNull(action);
								if(nextNode == null)
									nextNode = stackNode.getNewChildAfter(action);
								//System.out.println("6");
								if(nextNode instanceof InnerNode){
									stack.push((InnerNode)nextNode);
									if (EXPORT)
										writer.println("\"" + stackNode.hashCode() + "\" -> \"" + nextNode.hashCode()+ "\" [label=\"" + ((GGPAction)action).getTerm().toString() + "\"];");
								}
							}
						}			
					}
				}
				else{
					//System.out.println("7");
					if(stackNode.getGameState().isPlayerToMoveNature()){
						randomTurn = 1.00/(stackNode.getActions().size());				
					}
					else{
						parentNode = stackNode;
						isOpponentNode = true;
					}					
					for (Action action : stackNode.getActions()){	
						//System.out.println("8");
						if(isOpponentNode)
							parentAction = action;
						if(stackNode.getChildren()==null)
							nextNode = stackNode.getNewChildAfter(action);
						else	
							nextNode = stackNode.getChildOrNull(action);
						if(nextNode == null)
							nextNode = stackNode.getNewChildAfter(action);
						if(nextNode instanceof InnerNode){
							//findAllNodesInIS(perceptHash, nextIS,nextBelief,(InnerNode)nextNode, parentNode, parentAction, rootBelief, randomTurn);
							stack.push((InnerNode)nextNode);
							if (EXPORT)
								writer.println("\"" + stackNode.hashCode() + "\" -> \"" + nextNode.hashCode()+ "\" [label=\"" + ((GGPAction)action).getTerm().toString() + "\"];");
						}
					}
				}
				
			}
			
			
			index++;	
			if(leadsToNextIS)
				currentBeliefsSum += rootBelief;
			
			
			
		}
		
		
		
		//System.out.println("9");
		
		index = 0;
		for(InnerNode node : nextIS){
			nextBelief.set(index, nextBelief.get(index)/(currentBeliefsSum)); // no longer nSum here
			if(nextBelief.get(index)==0)
				System.out.printf("[Shodan V] Err: State with zero belief.\n");
			nextBeliefsSum += nextBelief.get(index);
			node.setParent(null);
			index++;
		}
		for(int i = 0; i < nextIS.size(); i++)
			nextBelief.set(i, nextBelief.get(i)/nextBeliefsSum);
		
		index=0;
		for(InnerNode node : nextIS){
			if(EXPORT)writer.println("\""+node.hashCode()+"\" [label=\""+nextBelief.get(index).toString()+"\"];");
			index++;
		}
		
		
		belief = nextBelief;
		return nextIS;
	}

	@Override
	public void stop() throws StoppingException {
		if(EXPORT) writer.println("}");
		writer.close();
		gameInfo = null;
	    isPlayable = true;
	    rootState = null;
	    expander = null;
	    gameTreeDepth = 0;
	    myself = null;
	    myselfRole = null;
	    myselfIndex = -1;
	    currentIS = null;
	    belief = null;
	    actionsPlayed = null;	    
	    ggpMCTSConfig = null;
	    runner = null;	    
	    strategy = null;	    
	    generator = null;
	    beliefSum = 0.0;
	    nSum = 0;
	    System.gc();
		System.out.println("[Shodan V] Game ended.");
	}

	@Override
	public void abort() throws AbortingException {
		// TODO Auto-generated method stub
		System.out.println("[Shodan V] Game aborted.");
	}

	@Override
	public void preview(Game g, long timeout) throws GamePreviewException {
		// no need
		
	}

	@Override
	public String getName() {
		return "Shodan Go";
	}

}
