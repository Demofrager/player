package cz.agents.gtlibrary.ggp.gdlII.player;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Set;

import org.ggp.base.player.gamer.Gamer;
import org.ggp.base.player.gamer.exception.AbortingException;
import org.ggp.base.player.gamer.exception.GamePreviewException;
import org.ggp.base.player.gamer.exception.MetaGamingException;
import org.ggp.base.player.gamer.exception.MoveSelectionException;
import org.ggp.base.player.gamer.exception.StoppingException;
import org.ggp.base.util.game.Game;
import org.ggp.base.util.gdl.grammar.GdlTerm;
import org.ggp.base.util.statemachine.Role;

import cz.agents.gtlibrary.algorithms.mcts.MCTSConfig;
import cz.agents.gtlibrary.algorithms.mcts.MCTSInformationSet;
import cz.agents.gtlibrary.algorithms.mcts.MCTSRunner;
import cz.agents.gtlibrary.algorithms.mcts.Simulator;
import cz.agents.gtlibrary.algorithms.mcts.distribution.FrequenceDistribution;
import cz.agents.gtlibrary.algorithms.mcts.nodes.InnerNode;
import cz.agents.gtlibrary.algorithms.mcts.nodes.Node;
import cz.agents.gtlibrary.algorithms.mcts.selectstrat.Exp3BackPropFactory;
import cz.agents.gtlibrary.ggp.gdlII.domain.prover.GGPAction;
import cz.agents.gtlibrary.ggp.gdlII.domain.prover.GGPExpander;
import cz.agents.gtlibrary.ggp.gdlII.domain.prover.GGPGameInfo;
import cz.agents.gtlibrary.ggp.gdlII.domain.prover.GGPGameState;
import cz.agents.gtlibrary.iinodes.ArrayListSequenceImpl;
import cz.agents.gtlibrary.interfaces.Action;
import cz.agents.gtlibrary.interfaces.Player;
import cz.agents.gtlibrary.interfaces.Sequence;
import cz.agents.gtlibrary.strategy.Strategy;
import cz.agents.gtlibrary.strategy.UniformStrategyForMissingSequences;

public class ShodanNI extends Gamer{
	
    private static final int MCTS_ITERATIONS_PER_CALL = (int)10;
    private static final double MCTSEXP3constant = 0.35;
    private static double gamma;// = 0.35;
	
    private GGPGameInfo gameInfo;
    private boolean isPlayable;
    private GGPGameState rootState;
    private GGPExpander<MCTSInformationSet> expander;
    
    private int gameTreeDepth;
    private Player myself;
    private Role myselfRole;
    private int myselfIndex;
    
    private ArrayList<InnerNode> currentIS;
    private ArrayList<Double> belief;
    private Sequence actionsPlayed;
    
    private MCTSConfig ggpMCTSConfig;
    private MCTSRunner runner;
    private Strategy strategy;
    
    private Random generator;
    
    //////////
	int perceptHash;
	double randomTurn;
	double rootBelief;
	boolean leadsToNextIS;
	ArrayList<InnerNode> nextIS;
	ArrayList<Double> nextBelief;
	InnerNode opponentNode;
	Action opponentAction;
	//////////
    
    
	@Override
	public void metaGame(long timeout) throws MetaGamingException {
		gameInfo = null;
		try {
			gameInfo = new GGPGameInfo(getMatch().getGame().getRules());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		System.out.println("[Shodan II] I AM ... Shodan.");
		
		myself = GGPGameInfo.getPlayerByRole(getRoleName());
        myselfIndex = GGPGameInfo.getRoleIndexByRole(getRoleName());
        myselfRole = GGPGameInfo.roles.get(myselfIndex);
		gameTreeDepth = 0;
		
		gamma = MCTSEXP3constant;
		System.out.printf("[Shodan II] Gamma set to: %f\n",gamma);
		
		if(myself == null || myselfRole==null)
			System.out.println("[Shodan II] Err: Couldn't instantiate roles.");
		
        if(GGPGameInfo.ALL_PLAYERS.length <= 3)
        	if(GGPGameInfo.randomPlayerIndex == -1 && GGPGameInfo.ALL_PLAYERS.length == 3)
        		isPlayable = false;
        	else isPlayable = true;
		else
			isPlayable = false;
        
        rootState = new GGPGameState();
        ggpMCTSConfig = new MCTSConfig(new Simulator(), new Exp3BackPropFactory(0,gameInfo.getMaxUtility(),gamma), new UniformStrategyForMissingSequences.Factory(), null);
        expander = new GGPExpander<MCTSInformationSet> (ggpMCTSConfig);
		runner = new MCTSRunner(ggpMCTSConfig, rootState, expander);
		
		runner.runMCTS(0, myself); // this creates a rootstate 
		
		currentIS = new ArrayList<InnerNode>();
		belief = new ArrayList<Double>();
		actionsPlayed = new ArrayListSequenceImpl(myself);
		generator = new Random();
		
		belief.add((double)1.00);
		currentIS.add(runner.getRootNode());
		
		while((timeout - System.currentTimeMillis()) > (2000)){
			runner.runMCTS(MCTS_ITERATIONS_PER_CALL, myself);
		}
        
        System.out.println("[Shodan II] Ready.");
	}

	@Override
	public GdlTerm selectMove(long timeout) throws MoveSelectionException {
		GGPAction action;
		GdlTerm move;
		int iteration = 0;
		long startTime;
		long oneRoundTime = 1;
		
		System.out.println("[Shodan II] Next Round!");
		System.out.printf("\t    Current percept history: %s \n",getMatch().getPerceptHistory().toString());
		currentIS = findCurrentInformationSet();
		System.out.printf("\t    IS size: %d \n",currentIS.size());
		
		if(!isPlayable) 
			action = (GGPAction)currentIS.get(0).getActions().get(0);
		else{
			System.out.println("[Shodan II] Running MCts.");
			startTime = timeout - System.currentTimeMillis();
			while((timeout - System.currentTimeMillis()) > (10*oneRoundTime+2000)){
				runner.setRootNode(selectMCTSroot());
				runner.runMCTS(MCTS_ITERATIONS_PER_CALL, myself);
					oneRoundTime = (startTime - (timeout - System.currentTimeMillis()))/(iteration+1);
				iteration++;
			}
			//System.out.printf("\t      Number of iters: %d\n",iteration*MCTS_ITERATIONS_PER_CALL);
			System.out.println("[Shodan II] Computing strategy.");
			strategy = runner.runMCTS(0, myself, new FrequenceDistribution());
			//calculateStrategy();
			// select action
			System.out.println("[Shodan II] Selecting action.");
			action = selectNextAction();
		}
		move = action.getTerm();
		System.out.printf("\t    Playing: %s.\n",move.toString());
		gameTreeDepth++;
		actionsPlayed.addLast(action);
		return move;
	}
	
	// returns Node with distribution of beliefs in current IS
	private InnerNode selectMCTSroot(){
		double counter = (double) 0;
		double prob = generator.nextDouble();
		for(int i = 0; i< belief.size(); i++){
				counter+=belief.get(i);
				if(counter > prob)
					return currentIS.get(i);
			}
		System.out.println("[Shodan II] Err: No node selected.");
		return null;
	}
	
	private GGPAction selectNextAction(){
		double counter = (double) 0;
		double prob = generator.nextDouble();
		for(Sequence seq : strategy.keySet())
			if(seq.size()==1){
				counter+=strategy.get(seq);
				System.out.printf("%s : %f\n",seq.toString(),strategy.get(seq));
				if(counter > prob){
					return (GGPAction)seq.getFirst();
				}
			}	
		System.out.println("[Shodan II] Err: No action selected.");
		return null;
	}
	
	private double getOpponentRealization(InnerNode opponent, Action opponentAction){
		if(opponent==null)
			return 1.00;
		
		int playerIndex = ((GGPGameState)opponent.getGameState()).getCurrentPlayerIndex();	
		GGPAction action = (GGPAction)opponentAction;
		double sum = 0.0;
		double parentSum = 0.0;
		double realization;
		double n;
		Set<InnerNode> informationSet = opponent.getInformationSet().getAllNodes();
		for(InnerNode node : informationSet){
			//i++;
			parentSum += node.getSecurelyNbSamplesForPlayer(playerIndex);
			n = getNumberOfIterationsFor(node, action,playerIndex);
			if(n!=0)
				sum+=n;
			else{
				return 1.00/(double)opponent.getActions().size();
			}
		}
		realization = sum / parentSum;
		if(realization > 1.00 || realization == 0){
			return 1.00/(double)opponent.getActions().size();
		}
		return realization;
	}
	
	private double getNumberOfIterationsFor(InnerNode node, GGPAction action, int index){
		Node child;
		if(node.getChildren()==null)
			return 0;
		child = node.getChildOrNull(action);
		if(child!=null){
			if(child instanceof InnerNode){
				if(((InnerNode) child).hasNbElement(index))
					return ((InnerNode) child).getNbSamplesForPlayer(index);
			}
		}
		return 0;
	}
	
	private ArrayList<InnerNode> findCurrentInformationSet(){
		perceptHash = getMatch().getPerceptHistory().hashCode();
		int index = 0;
		double nextBeliefsSum = (double)0;
		double currentBeliefsSum = (double)0;
		randomTurn = (double)1.00;
		nextIS = new ArrayList<InnerNode>();
		nextBelief = new ArrayList<Double>();
		opponentNode = null;
		opponentAction = null;
		Node nextNode;
		
		for(InnerNode ISnode : currentIS){
			leadsToNextIS = false;
			rootBelief = belief.get(index);
			randomTurn = 1.00;
			opponentAction = null;
			opponentNode = null;
			
			if(gameTreeDepth>0){
				if(ISnode.getChildren()==null)
					nextNode = null;
				else	
					nextNode = ISnode.getChildOrNull(actionsPlayed.getLast());
				if(nextNode == null)
					nextNode = ISnode.getNewChildAfter(actionsPlayed.getLast());
				if(nextNode instanceof InnerNode){
					searchNextGameTreeLayer((InnerNode)nextNode);
					}
			}
			else
				searchNextGameTreeLayer(ISnode);
		
			index++;	
			if(leadsToNextIS)
				currentBeliefsSum += rootBelief;		
		}	
		index = 0;
		for(InnerNode node : nextIS){
			nextBelief.set(index, nextBelief.get(index)/(currentBeliefsSum));
			if(nextBelief.get(index)==0)
				System.out.printf("[Shodan II] Err: State with zero belief.\n");
			nextBeliefsSum += nextBelief.get(index);
			node.setParent(null);
			index++;
		}
		for(int i = 0; i < nextIS.size(); i++)
			nextBelief.set(i, nextBelief.get(i)/nextBeliefsSum);
		
		for(int i = 0; i < nextIS.size(); i++){
			System.out.printf("%s : %f \n", nextIS.get(i).getGameState().getHistory().toString(),nextBelief.get(i));
		}
		System.out.println();
		
		index=0;
		
		belief = nextBelief;
		return nextIS;
	}
	
	private void searchNextGameTreeLayer(InnerNode node){
		Node nextNode;
		boolean isOpponentNode = false;
			if(gameTreeDepth==((GGPGameState)node.getGameState()).getCurrentTreeDepth()){
				if(perceptHash != ((GGPGameState)node.getGameState()).getPerceptHistory().get(myselfRole).hashCode())
					return;
				else{		
					if(node.getGameState().getPlayerToMove().equals(myself)){
						//calculate raw belief
						nextBelief.add(rootBelief*randomTurn*getOpponentRealization(opponentNode, opponentAction));
						nextIS.add(node);
						leadsToNextIS = true;
					}
					else{
						if(node.getGameState().isPlayerToMoveNature()){
							randomTurn = 1.00/(node.getActions().size());							
						}
						else{
							opponentNode = node;
							isOpponentNode = true;
						}
						for (Action action : node.getActions()){	
							if(isOpponentNode)
								opponentAction = action;				
							if(node.getChildren()==null)
								nextNode = null;
							else	
								nextNode = node.getChildOrNull(action);
							if(nextNode == null)
								nextNode = node.getNewChildAfter(action);
							if(nextNode instanceof InnerNode){
								searchNextGameTreeLayer((InnerNode)nextNode);
								}
						}
						isOpponentNode = false;
					}			
				}
			}
			else{
				if(node.getGameState().isPlayerToMoveNature()){
					randomTurn = 1.00/(node.getActions().size());				
				}
				else{
					opponentNode = node;
					isOpponentNode = true;
				}					
				for (Action action : node.getActions()){	
					if(isOpponentNode)
						opponentAction = action;
					if(node.getChildren()==null)
						nextNode = node.getNewChildAfter(action);
					else	
						nextNode = node.getChildOrNull(action);
					if(nextNode == null)
						nextNode = node.getNewChildAfter(action);
					if(nextNode instanceof InnerNode){
						searchNextGameTreeLayer((InnerNode)nextNode);
						}
				}
				isOpponentNode=false;
			}
			
		}

	@Override
	public void stop() throws StoppingException {
		gameInfo = null;
	    isPlayable = true;
	    rootState = null;
	    expander = null;
	    gameTreeDepth = 0;
	    myself = null;
	    myselfRole = null;
	    myselfIndex = -1;
	    currentIS = null;
	    belief = null;
	    actionsPlayed = null;	    
	    ggpMCTSConfig = null;
	    runner = null;	    
	    strategy = null;	    
	    generator = null;
	    System.gc();
		System.out.println("[Shodan II] Game ended.");
	}

	@Override
	public void abort() throws AbortingException {
		System.out.println("[Shodan II] Game aborted.");
	}

	@Override
	public void preview(Game g, long timeout) throws GamePreviewException {
		// no need
		
	}

	@Override
	public String getName() {
		return "Shodan Prover";
	}

}
