package cz.agents.gtlibrary.ggp.player.client;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import org.ggp.base.apps.player.match.MatchPanel;
import org.ggp.base.apps.player.network.NetworkPanel;
import org.ggp.base.player.GamePlayerII;
import org.ggp.base.player.gamer.Gamer;
import org.ggp.base.util.ui.NativeUI;



//import cz.agents.gtlibrary.ggp.gdlII.player.FullSeqPlayer;
import cz.agents.gtlibrary.ggp.gdlII.player.ShoDanGo;
import cz.agents.gtlibrary.ggp.gdlII.player.ShoDanRokuI;
import cz.agents.gtlibrary.ggp.gdlII.player.ShoDanYon;
import cz.agents.gtlibrary.ggp.gdlII.player.ShodanICHI;
import cz.agents.gtlibrary.ggp.gdlII.player.ShodanNI;
// Armin Shodans
import cz.agents.gtlibrary.ggp.gdlII.player.ShoDanArminRandom;
import cz.agents.gtlibrary.ggp.gdlII.player.ShoDanArmin;
import cz.agents.gtlibrary.ggp.gdlII.player.ShoDanArminF;
import cz.agents.gtlibrary.ggp.gdlII.player.ShoDanArminF2;
import cz.agents.gtlibrary.ggp.gdlII.player.ShoDanArminF2TwoPlayer;
import cz.agents.gtlibrary.ggp.gdlII.player.ShoDanArminF2TP2Nash;
import cz.agents.gtlibrary.ggp.gdlII.player.ShoDanArminF2TPCoordination;
import cz.agents.gtlibrary.ggp.gdlII.player.ArminChooseMovePlayer;
import cz.agents.gtlibrary.ggp.gdlII.player.ArminRandom;


//THis is the file to run

@SuppressWarnings("serial")
public final class PlayerClientII extends JPanel
{
	private static void createAndShowGUI(PlayerClientII playerPanel)
	{
		JFrame frame = new JFrame("Game Player");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		frame.setPreferredSize(new Dimension(800, 300));
		frame.getContentPane().add(playerPanel);

		frame.pack();
		frame.setVisible(true);
	}

	public static void main(String[] args) throws IOException
	{
	    NativeUI.setNativeUI();

	    final PlayerClientII playerPanel = new PlayerClientII();
	    javax.swing.SwingUtilities.invokeLater(new Runnable()
	    {

		@Override
		public void run()
		{
		    createAndShowGUI(playerPanel);
		}
	    });
	}

	private final JButton createButton;
	private final JTabbedPane playersTabbedPane;

	private final JTextField portTextField;

	private final JComboBox<String> typeComboBox;

	private Integer defaultPort = 4001;

	private List<Class<? extends Gamer>> gamers = new ArrayList<Class<? extends Gamer>>();
	
	public PlayerClientII()
	{
		super(new GridBagLayout());

		portTextField = new JTextField(defaultPort.toString());
		typeComboBox = new JComboBox<String>();
		createButton = new JButton(createButtonMethod());
		playersTabbedPane = new JTabbedPane();

		portTextField.setColumns(15);

		gamers.add(ShodanICHI.class);
		gamers.add(ShoDanGo.class);
		gamers.add(ShodanNI.class);
		//gamers.add(ShoDanSan.class);
		gamers.add(ShoDanRokuI.class);
		gamers.add(ShoDanYon.class);
		gamers.add(ShoDanArminRandom.class);
		gamers.add(ShoDanArmin.class);
		gamers.add(ShoDanArminF.class);
		gamers.add(ShoDanArminF2.class);
		gamers.add(ShoDanArminF2TwoPlayer.class);
		gamers.add(ShoDanArminF2TP2Nash.class);
		gamers.add(ShoDanArminF2TPCoordination.class);
		gamers.add(ArminChooseMovePlayer.class);
		gamers.add(ArminRandom.class);
		
		//gamers.add(FullSeqPlayer.class);
		//gamers.add(ShoDanRokuII.class);

		
		//gamers.add(BPPlayer.class);
		//gamers.add(MctsUCTPlayer.class);
		//gamers.add(LegalPlayer.class);
		//gamers.add(SavannahPlayer.class);
		List<Class<? extends Gamer>> gamersCopy = new ArrayList<Class<? extends Gamer>>(gamers);
		for(Class<? extends Gamer> gamer : gamersCopy)
		{
			Gamer g;
			try {
				g = gamer.newInstance();
				typeComboBox.addItem(g.getName());
			} catch(Exception ex) {
			    gamers.remove(gamer);
			}
		}
		//typeComboBox.setSelectedItem("Shodan Yon");
		typeComboBox.setSelectedItem("Armin Choose move player");
		JPanel managerPanel = new JPanel(new GridBagLayout());
		managerPanel.setBorder(new TitledBorder("Manager"));

		managerPanel.add(new JLabel("Port:"), new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(20, 5, 5, 5), 5, 5));
		managerPanel.add(portTextField, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(20, 5, 5, 5), 5, 5));
		managerPanel.add(new JLabel("Type:"), new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 5, 5));
		managerPanel.add(typeComboBox, new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(5, 5, 5, 5), 5, 5));
		managerPanel.add(createButton, new GridBagConstraints(1, 2, 1, 1, 1.0, 1.0, GridBagConstraints.SOUTH, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 0, 0));

		JPanel playersPanel = new JPanel(new GridBagLayout());
		playersPanel.setBorder(new TitledBorder("Players"));

		playersPanel.add(playersTabbedPane, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(5, 5, 5, 5), 5, 5));

		this.add(managerPanel, new GridBagConstraints(0, 0, 1, 1, 0.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(5, 5, 5, 5), 5, 5));
		this.add(playersPanel, new GridBagConstraints(1, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(5, 5, 5, 5), 5, 5));
	}

	private AbstractAction createButtonMethod()
	{
		return new AbstractAction("Create")
		{

			@Override
			public void actionPerformed(ActionEvent evt)
			{
				try
				{
					int port = Integer.valueOf(portTextField.getText());
					String type = (String) typeComboBox.getSelectedItem();

					MatchPanel matchPanel = new MatchPanel();
					NetworkPanel networkPanel = new NetworkPanel();
					Gamer gamer = null;

					Class<?> gamerClass = gamers.get(typeComboBox.getSelectedIndex());
					try {
						gamer = (Gamer) gamerClass.newInstance();
					} catch(Exception ex) { throw new RuntimeException(ex); }

					gamer.addObserver(matchPanel);

					GamePlayerII player = new GamePlayerII(port, gamer);
					player.addObserver(networkPanel);
					player.start();

					JTabbedPane tab = new JTabbedPane();
					tab.addTab("Match", matchPanel);
					tab.addTab("Network", networkPanel);
					playersTabbedPane.addTab(type + " (" + player.getGamerPort() + ")", tab);
					playersTabbedPane.setSelectedIndex(playersTabbedPane.getTabCount()-1);

					defaultPort++;
					portTextField.setText(defaultPort.toString());
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		};
	}
}