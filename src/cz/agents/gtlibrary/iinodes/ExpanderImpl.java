package cz.agents.gtlibrary.iinodes;

import java.util.List;

import cz.agents.gtlibrary.interfaces.Action;
import cz.agents.gtlibrary.interfaces.AlgorithmConfig;
import cz.agents.gtlibrary.interfaces.Expander;
import cz.agents.gtlibrary.interfaces.GameState;
import cz.agents.gtlibrary.interfaces.InformationSet;

public abstract class ExpanderImpl<I extends InformationSet> implements Expander<I> {

	private static final long serialVersionUID = -2367393002316400229L;
	
	final private AlgorithmConfig<I> algConfig;

	public ExpanderImpl(AlgorithmConfig<I> algConfig) {
		this.algConfig = algConfig;
	}

	@Override
	public AlgorithmConfig<I> getAlgorithmConfig() {
		return algConfig;
	}
	
	@Override
	public List<Action> getActionsForUnknownIS(GameState gameState) {
		return getActions(algConfig.createInformationSetFor(gameState));
	}

	@Override
	public List<Action> getActions(I informationSet) {
		List<Action> actions = getActions(informationSet.getAllStates().iterator().next());
		for (Action a : actions) {
			a.setInformationSet(informationSet);
		}
		return actions;
	}
}
