package cz.agents.gtlibrary.iinodes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.ggp.base.util.gdl.grammar.GdlTerm;
import org.ggp.base.util.statemachine.Role;

import cz.agents.gtlibrary.ggp.gdlII.domain.prover.GGPGameInfo;
import cz.agents.gtlibrary.interfaces.Action;
import cz.agents.gtlibrary.interfaces.GameState;
import cz.agents.gtlibrary.interfaces.History;
import cz.agents.gtlibrary.interfaces.Player;
import cz.agents.gtlibrary.interfaces.Sequence;
import cz.agents.gtlibrary.utils.Pair;

public abstract class GGPGameStateProverImpl extends GameStateImpl {

	private static final long serialVersionUID = 7693972683606265891L;

	protected Map<Role,List<Set<GdlTerm>>> perceptHistory;

	public GGPGameStateProverImpl(Player[] players, List<Role> roles) {
		super(players);
		this.perceptHistory = new HashMap<Role,List<Set<GdlTerm>>>();
		List<Set<GdlTerm>> percepts;
		for(Role r : roles){
			percepts = new ArrayList<Set<GdlTerm>>();
			perceptHistory.put(r, percepts);
		}
	}

	public GGPGameStateProverImpl(GGPGameStateProverImpl gameState) {
		super(gameState);
		this.perceptHistory = perceptsDeepCopy(gameState.getPerceptHistory());
	}
	
	public Map<Role,List<Set<GdlTerm>>> getPerceptHistory(){
		return perceptHistory;
	}

	@Override
	public abstract Player getPlayerToMove();

	@Override
	public GameState performAction(Action action) {
		GGPGameStateProverImpl state = (GGPGameStateProverImpl) this.copy();

		state.performActionModifyingThisState(action);
		//if(GGPGameInfo.EXPORT) GGPGameInfo.writer.println( "\""+this.toString().substring(this.toString().length()-8) + ":" +this.perceptHistory.get(((GGPGameState)this).getPlayerRoleToMove()).toString()+":" +this.perceptHistory.get(((GGPGameState)this).getPlayerRoleToMove()).hashCode()+"\"" + " -> " + "\"" + state.toString().substring(state.toString().length()-8) + ":" +state.perceptHistory.get(((GGPGameState)state).getPlayerRoleToMove()).toString()+":" +state.perceptHistory.get(((GGPGameState)state).getPlayerRoleToMove()).hashCode()+"\"" + " [label= \"" +action.toString() + "\" ];");
		return state;
	}

	@Override
	public void performActionModifyingThisState(Action action) {
		if (isPlayerToMoveNature() || checkConsistency(action)) {
			updateNatureProbabilityFor(action);
			addActionToHistory(action, getPlayerToMove());
			action.perform(this);
		} else {
			throw new IllegalStateException("Inconsistent move.");
		}
	}

	private void updateNatureProbabilityFor(Action action) {
		if (isPlayerToMoveNature())
			natureProbability *= getProbabilityOfNatureFor(action);
	}

	private void addActionToHistory(Action action, Player playerToMove) {
		history.addActionOf(action, playerToMove);
	}

	@Override
	public boolean checkConsistency(Action action) {
		if (action == null || action.getInformationSet() == null)
			return false;
		boolean test = this.getISKeyForPlayerToMove().equals(new Pair<Integer, Sequence>(action.getInformationSet().hashCode(), action.getInformationSet().getPlayersHistory()));
		if(!test){
			//for(int i = 0; i< 14; i++)
				//System.out.println(action.getInformationSet().hashCode());
			System.out.println(this.getISKeyForPlayerToMove().toString());
			System.out.println((new Pair<Integer, Sequence>(action.getInformationSet().hashCode(), action.getInformationSet().getPlayersHistory())).toString());
		}
		return test;
		//		return action.getInformationSet().getAllStates().contains(this);
	}

	@Override
	public History getHistory() {
		return history;
	}

	@Override
	public Player[] getAllPlayers() {
		return players;
	}

	@Override
	public Sequence getSequenceFor(Player player) {
		return history.getSequenceOf(player);
	}

	@Override
	public Sequence getSequenceForPlayerToMove() {
		return history.getSequenceOf(getPlayerToMove());
	}

	@Override
	public double getNatureProbability() {
		return natureProbability;
	}

	@Override
	public abstract GameState copy();

	@Override
	public abstract double[] getUtilities();

	@Override
	public abstract boolean isGameEnd();

	@Override
	public abstract boolean isPlayerToMoveNature();

	@Override
	public void reverseAction() {
		throw new UnsupportedOperationException();
	}

	@Override
	public abstract int hashCode();

	@Override
	public abstract boolean equals(Object object);
	
	private Map<Role,List<Set<GdlTerm>>> perceptsDeepCopy( Map<Role,List<Set<GdlTerm>>> perceptHistory ){
		HashMap<Role,List<Set<GdlTerm>>> newPerceptHistory = new HashMap<Role,List<Set<GdlTerm>>>();
		List<Set<GdlTerm>> percepts;
		Set<GdlTerm> percept;
		
		for(Role r : perceptHistory.keySet()){
			percepts = new ArrayList<Set<GdlTerm>>();
			for(Set<GdlTerm> list : perceptHistory.get(r)){
				percept = new HashSet<GdlTerm>();
				//percept = GGPGameInfo.cloner.deepClone(list);
				for(GdlTerm term : list)
					percept.add(GGPGameInfo.cloner.deepClone(term));
				percepts.add(percept);
			}
			newPerceptHistory.put(r, percepts);
		}
			
		return newPerceptHistory;
	}
}
