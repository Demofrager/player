package cz.agents.gtlibrary.interfaces;

import java.io.Serializable;

public interface Player extends Serializable {
	public int getId();
}
