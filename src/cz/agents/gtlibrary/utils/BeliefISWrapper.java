package cz.agents.gtlibrary.utils;

import java.util.ArrayList;

import cz.agents.gtlibrary.algorithms.mcts.nodes.InnerNode;

public class BeliefISWrapper {

	private ArrayList<InnerNode> InformationSet;
	private  ArrayList<Double> belief;
	
	public BeliefISWrapper(ArrayList<InnerNode> inputIS, ArrayList<Double> inputBelief ) {
		InformationSet = inputIS;
		belief = inputBelief;
		
	}
	
	public ArrayList<InnerNode> getInformationSet(){
		return InformationSet;
	}
	
	public ArrayList<Double> getBelief(){
		return belief;
	}
	
	
}
