package cz.agents.gtlibrary.utils;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import cz.agents.gtlibrary.algorithms.mcts.nodes.InnerNode;
import cz.agents.gtlibrary.interfaces.Action;

/**
 * InformationSetArmin is an object which holds some a key and a set of states which belongs to it.
 * The keys are {Level, HashCode, SequenceOfSelfMove}  -- note: HashCode is converted to sequenceOFSelfMoveHash
 * Hash code is the perception. but since we don't have perception in the jakub representation of GGPII
 * and we only get perception when all player have moved so we add level. 
 * The level helps to separate states with the same perception and same list of our moves 
 *
 * Every thing in the key except level is converted into hash code and added as an integer
 * 
 * For the purpose of the coordinate player I will add an attribute of pastSequence 
 * 
 * @author arminchitizadeh
 *
 */
public class InformationSetClassArmin {
	
	private int level;
	private int perceptionHash;
	private int sequenceOfSelfMoveHash;
	private int value;
	private int randomChoices;
	//private Set<GdlTerm> actualPerception;
	private ArrayList<InnerNode> states;
	private Action chosenAction;
	//coordinate
	private int sequenceOfPastMovesHash;
	private LinkedList<Action> pastActions;
	
	
	public InformationSetClassArmin ( int inputLevel, int inputPerception, int inputSequenceOfSelfMoveHash, int inputSequenceOFpastMovesHash, LinkedList<Action> inputPastActions){
		this.level = inputLevel;
		this.sequenceOfPastMovesHash = inputSequenceOFpastMovesHash;
		this.perceptionHash = inputPerception;
		this.sequenceOfSelfMoveHash = inputSequenceOfSelfMoveHash;
		this.states = new ArrayList<InnerNode>();
		this.pastActions = inputPastActions;
	}
	
	public void print(){
		System.out.println("printing information Set:");
		if (states.size() == 0)	System.out.println("IS is empty!");
		for ( InnerNode in : states){
			System.out.println(in.getGameState().getHistory());
			System.out.println("\t\t percept Hash:" + this.perceptionHash);
			System.out.println("\t\t level:" + this.level);
			System.out.println("\t\t sequenceOfSelfMove" + this.sequenceOfSelfMoveHash);
		}		
	}
	
	
	public void setChosenAction ( Action inputAction){
		this.chosenAction = inputAction;
	}
	
	public Action getChosenAction(){
		return this.chosenAction;
	}
	
	public void setRandomChoices ( int inputRandomChoices){
		this.randomChoices = inputRandomChoices;
	}
	
	public int getRandomChoices(){
		return this.randomChoices;
	}
	
	public void addState(InnerNode inputNode){
		this.states.add(inputNode);
	}
	
	public boolean doesKeyMatch(int inputLevel, int inputPerceptionHash, int inputSequenceOfSelfMoveHash){
		/*System.out.println("<IN>");
		System.out.println("level" + (this.level == inputLevel));
		System.out.println("preception" + (this.perceptionHash == inputPerceptionHash));
		System.out.println("selfMoves" + (this.sequenceOfSelfMoveHash == inputSequenceOfSelfMoveHash));
		
		System.out.println("<IN END>");
		*/
		return  ((this.level == inputLevel) && (this.perceptionHash == inputPerceptionHash) && (this.sequenceOfSelfMoveHash == inputSequenceOfSelfMoveHash));
	}
	
	//coordinate
	public boolean doesKeyMatchCoordinate(int inputLevel, int inputPerceptionHash, LinkedList<Action> inputSequenceOfPastMoves){
		System.out.println("comparing: Level: " + inputLevel + " vs " + this.level );
		System.out.println("comparing: inputPerHash: " + inputPerceptionHash + " vs " + this.perceptionHash );
		System.out.println("comparing: inputSequenceOfPastMovesHash: " + inputSequenceOfPastMoves + " vs " + this.sequenceOfPastMovesHash );
		boolean pastActionEq = inputSequenceOfPastMoves.toString().equals(this.pastActions.toString());
		return  ((this.level == inputLevel) && (this.perceptionHash == inputPerceptionHash) && pastActionEq);
	}
	
	
	public LinkedList<Action> getPastActions(){
		return this.pastActions;
	}
	
	public int getPastMovesHash(){
		return this.sequenceOfPastMovesHash;
	}
	
	public ArrayList<InnerNode> getStates(){
		return this.states;
	}
	
	public void setValue(int inputValue){
		this.value = inputValue;
	}
	
	public int getValue(){
		return this.value;
	}
	
	public int getPerception(){
		return this.perceptionHash;
	}
	
	public int getSequenceOfSelfMoveHash(){
		return this.sequenceOfSelfMoveHash;
	}
	
	public int getLevel(){
		return this.level;
	}
	
	
}
