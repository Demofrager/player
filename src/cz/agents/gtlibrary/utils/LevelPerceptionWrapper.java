package cz.agents.gtlibrary.utils;

import java.util.ArrayList;

import cz.agents.gtlibrary.algorithms.mcts.nodes.InnerNode;

public class LevelPerceptionWrapper {

	private int perceptionHash;
	private  int level;
	
	public LevelPerceptionWrapper(int perceptionHashInput, int levelInput ) {
		perceptionHash = perceptionHashInput;
		level = levelInput;
		
	}
	
	public int getLevel(){
		return this.level;
	}
	
	public int getPerceotionHash(){
		return this.perceptionHash;
	}
	
	
}
