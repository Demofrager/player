package cz.agents.gtlibrary.utils;

/**
 * This is just a static class which I use to do all the dirty math works here
 * Just to make the actual code cleaner
 * @author arminchitizadeh
 *
 */
public class MathThing {
	
	
	static public double[] addTwoArray(double[] first, double[] second){
		
		if (first == null){
			return second;
		}
		else if( second == null){
			return first;
		}
		else{
			double[] answer = new double[first.length];
			for (int i=0 ; i < first.length ; i++){
				answer[i] = first[i] + second[i];
			}	
			return answer;
		}
	}

	static public double[] devideArraysElements( double[] theArray, int number){
		double[] answer = new double[theArray.length];
		for (int i=0 ; i < theArray.length ; i++){
			answer[i] = theArray[i] / number;
		}
		return answer;
	}
	
}
