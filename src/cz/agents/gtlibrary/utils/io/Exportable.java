package cz.agents.gtlibrary.utils.io;

public interface Exportable {
	
	public String getColumnLabels();
	
	public String getColumnValues();

}
