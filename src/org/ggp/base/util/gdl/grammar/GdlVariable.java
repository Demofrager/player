package org.ggp.base.util.gdl.grammar;

@SuppressWarnings("serial")
public final class GdlVariable extends GdlTerm
{

	private final String name;

	GdlVariable(String name)
	{
		this.name = name.intern();
	}

	public String getName()
	{
		return name;
	}

	@Override
	public boolean isGround()
	{
		return false;
	}

	@Override
	public GdlSentence toSentence()
	{
		throw new RuntimeException("Unable to convert a GdlVariable to a GdlSentence!");
	}

	@Override
	public String toString()
	{
		return name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GdlVariable other = (GdlVariable) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
	

}
