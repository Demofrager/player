package org.ggp.base.util.statemachine;

import java.util.LinkedList;


public class MachineStateNode {


	private MachineStateNode parent; // this reflect to parent which is same as this node
	private MachineState value; //this is the state
	private Move move; // This is the move which took from parents to here.


	public MachineStateNode( ){

	}


	/*
	 * It will print the moves from the last move to the start move
	 * it is just for TESTING DO NOT USE IT USE
	 * printMovesStartToEnd()
	 *
	 */
	public void printMovesEndToStart(){
		MachineStateNode tempNode = this;
		while(tempNode.getMove() != null){
			System.out.println("M.O.V.E:" + tempNode.getMove().toString());
			tempNode = tempNode.getParent();
		}

	}


	/**
	 * After finding the final state run this method to get the ordered list of moves
	 * Need to use answer.removeFirst() to remove moves in orders
	 * @return LinkedList<Move> which need to be used like
	 * answerList = finalTempNode.getMovesStartToEnd()
	 * while (!answerList.Empty){
	 *      dosomething (answerList.removeFirst())
	 * }
	 */
	public LinkedList<Move> getMovesStartToEnd(){
		LinkedList<Move> answer = new LinkedList<Move>();


		MachineStateNode tempNode = this;
		while(tempNode.getMove() != null){
			answer.addFirst(tempNode.getMove());
			tempNode = tempNode.getParent();
		}
		///???????????
		//answer.removeFirst(); // the first move is not needed ??
		///???????????
		return answer;

	}

	public void setParent(MachineStateNode p){
		this.parent = p;
	}

	public void setValue(MachineState ms){
		this.value = ms;
	}

	public void setMove(Move m){
		this.move = m;
	}

	public MachineState getValue(){
		return this.value;
	}

	public MachineStateNode getParent(){
		return this.parent;
	}

	public Move getMove(){
		return this.move;
	}

}
