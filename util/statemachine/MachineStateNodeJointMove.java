package org.ggp.base.util.statemachine;

import java.util.LinkedList;
import java.util.List;


public class MachineStateNodeJointMove  implements Comparable<MachineStateNodeJointMove> {


	private MachineStateNodeJointMove parent; // this reflect to parent which is same as this node
	private MachineState value; //this is the state
	private Move move; // This is the move which took from parents to here.
	private List<Move> jointMove;

	public MachineStateNodeJointMove( ){

	}


	/**
	 * It will print the moves from the last move to the start move
	 * it is just for TESTING DO NOT USE IT USE
	 * printMovesStartToEnd()
	 *
	 */
	public void printMovesEndToStart(){
		MachineStateNodeJointMove tempNode = this;
		while(tempNode.getMove() != null){
			System.out.println("M.O.V.E:" + tempNode.getMove().toString());
			tempNode = tempNode.getParent();
		}

	}


	/**
	 * After finding the final state run this method to get the ordered list of moves
	 * Need to use answer.removeFirst() to remove moves in orders
	 * @return LinkedList<Move> which need to be used like
	 * answerList = finalTempNode.getMovesStartToEnd()
	 * while (!answerList.Empty){
	 *      dosomething (answerList.removeFirst())
	 * }
	 */
	public LinkedList<Move> getMovesStartToEnd(){
		LinkedList<Move> answer = new LinkedList<Move>();


		MachineStateNodeJointMove tempNode = this;
		while(tempNode.getMove() != null){
			answer.addFirst(tempNode.getMove());

			tempNode.printJointMove();


			tempNode = tempNode.getParent();

		}
		///???????????
		//answer.removeFirst(); // the first move is not needed ??
		///???????????
		return answer;

	}

	public void setJointMovie(List<Move> jm){
		this.jointMove = jm;
	}

	public void setParent(MachineStateNodeJointMove p){
		this.parent = p;
	}

	public void setValue(MachineState ms){
		this.value = ms;
	}

	public void setMove(Move m){
		this.move = m;
	}

	public List<Move> getJointMove(){
		return this.jointMove;
	}

	public MachineState getValue(){
		return this.value;
	}

	public MachineStateNodeJointMove getParent(){
		return this.parent;
	}

	public Move getMove(){
		return this.move;
	}

	public void printJointMove(){
		System.out.print("The Joint Moves <<");

		for ( Move m : this.jointMove){
			System.out.print(m.toString() + ",");
		}
		System.out.println(">>");
	}

	@Override
	public int compareTo(MachineStateNodeJointMove compareUser){


		return this.getJointMove().toString().compareTo(compareUser.getJointMove().toString());
	}



}
